FROM openjdk:17 as builder
ADD . .
RUN ./gradlew --no-daemon shadowJar

FROM openjdk:17
COPY --from=builder app/build/libs/app-all.jar app-all.jar
ADD config config
ENTRYPOINT ["java", "--add-opens", "java.base/java.lang=ALL-UNNAMED", "-jar", "app-all.jar"]

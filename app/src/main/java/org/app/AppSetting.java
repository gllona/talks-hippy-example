package org.app;

import lombok.RequiredArgsConstructor;

import java.util.Arrays;
import java.util.List;

import static org.app.AppSetting.Source.APP;
import static org.app.AppSetting.Source.MATRIX;

@RequiredArgsConstructor
public enum AppSetting {
    HELLO_WORLD(APP),
    FLYWAY_URL(APP),
    FLYWAY_USER(APP),
    FLYWAY_PASSWORD(APP),
    DB_RDBMS(APP),
    DB_HOST(APP),
    DB_PORT(APP),
    DB_DATABASE(APP),
    DB_USERNAME(APP),
    DB_PASSWORD(APP),
    DB_OPTIONS(APP),
    DB_CONNECTION_POOL_SIZE(APP),
    DB_CONNECTION_POOL_TIMEOUT_IN_MILLIS(APP),
    DB_CONNECTION_TTL_IN_MILLIS(APP),
    DB_KEEPALIVE_QUERY(APP),
    TALKS_GROUP(APP),
    MAIN_TALK(APP),
    CLIENT_BOT_USERS_ARE_OPERATORS(APP),
    CLIENT_DEFAULT_CODE(APP),
    KEYWORD_DEFAULT(APP),
    REST_CONTROLLER_PORT(APP),
    TELEGRAM_ENABLED(APP),
    MATRIX_ENABLED(APP),
    HEALTH_ENDPOINTS_API_KEY(APP),
    HEALTH_ENDPOINT(APP),
    MATRIX_ENDPOINTS_API_KEY(MATRIX),
    MATRIX_ENDPOINT_RECEIVE_MESSAGE(MATRIX),
    MATRIX_ENDPOINT_GET_MESSAGES(MATRIX),
    MATRIX_ENDPOINT_CONFIRM_MESSAGES(MATRIX),
    MATRIX_ENDPOINT_TAG_ROOM(MATRIX),
    MAGIC_CODE(APP);

    private final Source source;

    @RequiredArgsConstructor
    enum Source {
        APP("app"),
        TELEGRAM("telegram"),
        MATRIX("matrix");

        private final String propertiesFileSuffix;
    }

    public static boolean isSourceEnabled(Source source) {
        return switch (source) {
            case APP -> true;
            case TELEGRAM -> "1".equals(AppConfig.getInstance().getString(AppSetting.TELEGRAM_ENABLED));
            case MATRIX -> "1".equals(AppConfig.getInstance().getString(AppSetting.MATRIX_ENABLED));
        };
    }

    public static String get(AppSetting setting) {
        return isSourceEnabled(setting.source) ?
            AppConfig.getInstance().getString(setting) :
            null;
    }

    public static Integer getInteger(AppSetting setting) {
        try {
            return Integer.parseInt(get(setting));
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static boolean isEnabled(AppSetting setting) {
        return 1 == getInteger(setting);
    }

    public static List<AppSetting> getEnabledSettings() {
        return Arrays.stream(values())
            .filter(setting -> isSourceEnabled(setting.source))
            .toList();
    }

    public static String getPropertiesFileSuffix(AppSetting setting) {
        return setting.source.propertiesFileSuffix;
    }

    public static String unencode(String text) {
        return AppConfig.getInstance().unencode(text);
    }
}

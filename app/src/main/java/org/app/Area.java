package org.app;

import lombok.Getter;

import java.util.stream.Stream;

public enum Area {

    MAIN("Main");

    @Getter private final String stdName;

    Area(String stdName) {
        this.stdName = stdName;
    }

    public static Area from(String name) {
        String stdName = Utils.normalizeInternationalCharacters(name);
        Area area = Stream.of(values())
            .filter(c -> c.stdName.equalsIgnoreCase(stdName))
            .findFirst()
            .orElse(null);
        return area;
    }
}

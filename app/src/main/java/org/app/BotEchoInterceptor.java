package org.app;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.List;
import java.util.Optional;

public class BotEchoInterceptor extends Interceptor {

    private final Logger logger = LogManager.getLogger(BotEchoInterceptor.class);

    public BotEchoInterceptor(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.empty();
    }

    @Override
    public List<Talk> interceptedTalks() {
        return TALKS_ALL;
    }

    @Override
    public InterceptorResponse intercept(TalkRequest request) {
        String username = request.getUsername();
        return Usernames.isBot(username) ?
            processBotRequest(request) :
            allowResponses(request);
    }

    private InterceptorResponse processBotRequest(TalkRequest request) {
        return blockResponses(request);
    }

    private InterceptorResponse blockResponses(TalkRequest request) {
        return InterceptorResponse.ofLastResponse(this, false);
    }

    private InterceptorResponse allowResponses(TalkRequest request) {
        return InterceptorResponse.ofLastResponse(this, true);
    }
}

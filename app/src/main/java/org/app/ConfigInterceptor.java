package org.app;

import org.logicas.librerias.talks.api.ConfigInterceptorApi;
import org.logicas.librerias.talks.api.TalksConfiguration;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Supplier;

public class ConfigInterceptor implements ConfigInterceptorApi {

    private static ConfigInterceptor instance;

    private TalksConfiguration talksConfig; // can be used in the future to get some values from DB
    private ConcurrentMap<String, String> settingsMap = new ConcurrentHashMap<>();

    public static synchronized ConfigInterceptor getInstance(TalksConfiguration talksConfig) {
        if (instance == null) {
            instance = new ConfigInterceptor(talksConfig);
        }
        return instance;
    }

    private ConfigInterceptor(TalksConfiguration talksConfig) {
        this.talksConfig = talksConfig;
    }

    @Override
    public String getProperty(String settingName, Supplier<String> defaultSupplier) {
        String value = settingsMap.get(settingName);
        return value != null ? value : defaultSupplier.get();
    }

    public void setPropertyValue(String settingName, String value) {
        settingsMap.put(settingName, value);
    }

    public void dropPropertyValue(String settingName) {
        settingsMap.remove(settingName);
    }
}

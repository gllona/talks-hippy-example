package org.app;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class Metrics {

    private static final String FORMAT = "METRIC category=%s value=%s tags=\"%s\"";

    private static final Logger logger = LogManager.getLogger(Metrics.class);

    public static void event(String category, String value, Object ...tags) {
        logger.info(String.format(FORMAT, category, value, formatTags(tags)));
    }

    private static String formatTags(Object[] tags) {
        return Arrays.stream(tags)
            .map(tag -> tag == null ? null : tag.toString().replace("\"", "'"))
            .toList()
            .toString();
    }
}

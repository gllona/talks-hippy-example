package org.app;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.config.LibSetting;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhoneNumbers {

    private static final PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

    private static final String PRELOADED_SESSION_ID_OF_PHONE_NUMBER = "phone::all::%s";
    private static final Pattern MATRIX_USERNAME_REGEX_PATTERN = Pattern.compile(String.format(
        "@(telegram|whatsapp|signal)_([0-9]+):%s", LibSetting.get(LibSetting.MATRIX_SERVER)));
    private static final Pattern LOOSE_PHONE_NUMBER_REGEX_PATTERN = Pattern.compile("^ *\\+([0-9]+ +)?[0-9]+ *$");

    private static final Logger logger = LogManager.getLogger(PhoneNumbers.class);

    public static String normalized(String sourcePhoneNumber) {
        if (sourcePhoneNumber == null || ! LOOSE_PHONE_NUMBER_REGEX_PATTERN.matcher(sourcePhoneNumber).matches()) {
            return null;
        }
        String phoneNumber = sourcePhoneNumber.trim();
        phoneNumber = phoneNumber.startsWith("+") ? phoneNumber : "+" + phoneNumber;
        phoneNumber = phoneNumber.replace(" ", "");
        return phoneNumber;
    }

    public static Integer getCountryCode(String phoneNumber) {
        Phonenumber.PhoneNumber phoneNumberProto = getPhoneNumberProto(phoneNumber);
        return phoneNumberProto == null ? null : phoneNumberProto.getCountryCode();
    }

    public static Long getLocalNumber(String phoneNumber) {
        Phonenumber.PhoneNumber phoneNumberProto = getPhoneNumberProto(phoneNumber);
        return phoneNumberProto == null ? null : phoneNumberProto.getNationalNumber();
    }

    public static boolean userIdMatchesPhoneNumber(String phoneNumber, String userId) {
        Integer countryCode = PhoneNumbers.getCountryCode(phoneNumber);
        Long localPhoneNumber = PhoneNumbers.getLocalNumber(phoneNumber);
        Pattern pattern = Pattern.compile(String.format("@(whatsapp|signal|telegram)_%s[0-9]*%s:%s",
            countryCode, localPhoneNumber, LibSetting.get(LibSetting.MATRIX_SERVER)));
        return pattern.matcher(userId).matches();
    }

    public static boolean preloadedSessionIdMatchesPhoneNumber(String phoneNumber, String sessionId) {
        Integer countryCode = PhoneNumbers.getCountryCode(phoneNumber);
        Long localPhoneNumber = PhoneNumbers.getLocalNumber(phoneNumber);
        if (countryCode == null || localPhoneNumber == null) {
            return false;
        }
        String localPhoneNumberStr = localPhoneNumber.toString();
        String localPhoneNumberFirstDigitRegex = "";
        if (localPhoneNumberStr.length() > 1) {
            localPhoneNumberFirstDigitRegex = localPhoneNumberStr.charAt(0) + "?";
            localPhoneNumberStr = localPhoneNumberStr.substring(1);
        }
        String phoneNumberRegex = String.format("%d[0-9]*%s%s", countryCode, localPhoneNumberFirstDigitRegex, localPhoneNumberStr);
        Pattern pattern = Pattern.compile(String.format(PRELOADED_SESSION_ID_OF_PHONE_NUMBER, phoneNumberRegex));
        return pattern.matcher(sessionId).matches();
    }

    public static String preloadedSessionIdOf(String phoneNumber) {
        String phone = normalized(phoneNumber);
        return phone == null ? null : String.format(PRELOADED_SESSION_ID_OF_PHONE_NUMBER, phone.substring(1));
    }

    public static String phoneNumberOf(String matrixUsername) {
        Matcher matcher = MATRIX_USERNAME_REGEX_PATTERN.matcher(matrixUsername);
        if (! matcher.matches()) {
            return null;
        }
        return "+" + matcher.group(2);
    }

    private static Phonenumber.PhoneNumber getPhoneNumberProto(String phoneNumber) {
        if (phoneNumber == null) {
            return null;
        }
        String phone = normalized(phoneNumber);
        if (phone == null) {
            return null;
        }
        Phonenumber.PhoneNumber phoneNumberProto = null;
        try {
            phoneNumberProto = phoneNumberUtil.parse(phoneNumber, "US");
        } catch (NumberParseException e) {
            logger.error(String.format("Can not recognize phone number [%s]", phoneNumber));
            return null;
        }
        return phoneNumberProto;
    }
}

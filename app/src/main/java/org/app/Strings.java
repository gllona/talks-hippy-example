package org.app;

import org.logicas.librerias.talks.localization.BaseStrings;

public class Strings extends BaseStrings {

    private static Strings instance;

    public static synchronized Strings getInstance(BotLangGetter botLangGetter) {
        if (instance == null) {
            instance = new Strings(botLangGetter);
        }
        return instance;
    }

    protected Strings(BotLangGetter botLangGetter) {
        super(botLangGetter);
    }

    public enum Lang {
        EN,
        ES;
    }

    public enum Str {
        HelloWorld,
        YourNameIs
    }

    protected void configureEnumClasses() {
        setLangs(Lang.class);
        setStrings(Str.class);
    }

    protected Lang getDefaultLang() {
        return Lang.EN;
    }

    protected void setStrings() {
        builder(Str.HelloWorld)
            .with(Lang.EN, "Hello world!")
            .with(Lang.ES, "Hola mundo!")
            .build();
        builder(Str.YourNameIs)
            .with(Lang.EN, "Your name is {userName}")
            .with(Lang.ES, "Tu nombre es {userName}")
            .build();
    }
}

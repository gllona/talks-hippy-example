package org.app;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Getter
@RequiredArgsConstructor
public enum TalkGroups {

    ALL(
        List.of(Talks.values())
    );

    private final List<Talks> enabledTalks;
}

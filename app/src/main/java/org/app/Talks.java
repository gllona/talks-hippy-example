package org.app;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.app.admin.AdminTalk;
import org.app.admin.AuthTalk;
import org.app.admin.SettingsTalk;
import org.app.buttons.Buttons;
import org.app.communicator.CommunicatorTalk;
import org.app.communicator.ContactTalk;
import org.app.concurrency.ConcurrencyTalk;
import org.app.eatings.Eatings;
import org.app.eatings.EatingsToo;
import org.app.echo.EchoTalk;
import org.app.emoji.Emoji;
import org.app.flyingmessages.FlyingMessages;
import org.app.groups.JoinGroup;
import org.app.groups.LeaveGroup;
import org.app.handlers.Handlers;
import org.app.hangman.HangmanTalk;
import org.app.hints.Hints;
import org.app.html.Html;
import org.app.interceptor.InterceptedTalk;
import org.app.interceptor.InterceptorTalk;
import org.app.localisation.DrosophilaMelanogaster;
import org.app.main.Fallback;
import org.app.main.Main;
import org.app.main.MainTalk;
import org.app.media.Media;
import org.app.messageids.MessageIds;
import org.app.priorities.Priorities;
import org.app.xarxa.*;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.engine.Talk;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@Getter
public enum Talks implements TalkType {

    // Important: All enum elements should have the same name and capitalization as the class name
    BotEchoInterceptor(BotEchoInterceptor.class),
    Main(Main.class),
    MainTalk(MainTalk.class),
    Eatings(Eatings.class),
    EatingsToo(EatingsToo.class),
    Xarxa(Xarxa.class),
    XarxaPersonas(XarxaPersonas.class),
    XarxaOfrecimientos(XarxaOfrecimientos.class),
    XarxaPeticiones(XarxaPeticiones.class),
    XarxaSoportes(XarxaSoportes.class),
    Media(Media.class),
    Buttons(Buttons.class),
    HTML(Html.class),
    Hints(Hints.class),
    Handlers(Handlers.class),
    Priorities(Priorities.class),
    ConcurrencyTalk(ConcurrencyTalk.class),
    InterceptedTalk(InterceptedTalk.class),
    InterceptorTalk(InterceptorTalk.class),
    EchoTalk(EchoTalk.class),
    Emoji(Emoji.class),
    DrosophilaMelanogaster(DrosophilaMelanogaster.class),
    FlyingMessages(FlyingMessages.class),
    MessageIds(MessageIds.class),
    HangmanTalk(HangmanTalk.class),
    JoinGroup(JoinGroup.class),
    LeaveGroup(LeaveGroup.class),
    AdminTalk(AdminTalk.class),
    AuthTalk(AuthTalk.class),
    SettingsTalk(SettingsTalk.class),
    CommunicatorTalk(CommunicatorTalk.class),
    ContactTalk(ContactTalk.class),
    Fallback(Fallback.class);

    private Class<? extends Talk> talkClass;

    public static List<TalkType> getAllTalkTypes() {
        String talksGroup = AppSetting.get(AppSetting.TALKS_GROUP);
        return (StringUtils.isEmpty(talksGroup)) ?
            buildTalkTypes() :
            buildTalkTypes(TalkGroups.valueOf(talksGroup));
    }

    private static List<TalkType> buildTalkTypes() {
        return Arrays.stream(values())
            .map(t -> (TalkType) t)
            .collect(Collectors.toList());
    }

    private static List<TalkType> buildTalkTypes(TalkGroups talkGroup) {
        return talkGroup.getEnabledTalks().stream()
            .map(t -> (TalkType) t)
            .collect(Collectors.toList());
    }

    @Override
    public Class<? extends Talk> getTalkClass() {
        return talkClass;
    }

    @Override
    public String getName() {
        return talkClass.getSimpleName();
    }
}

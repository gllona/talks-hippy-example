package org.app;

public class Triggers {

    public static final String NOT_A_COMMAND = "[^/][\\s\\S]*";
    public static final String FOUR_DIGITS = "[0-9]{4}";
    public static final String POSITIVE_INTEGER = "[0-9]{1,8}";
    public static final String VERSION = "v[0-9]{1,4}";
    public static final String GENERATION = "v[0-9]{1,4}";
    public static final String IDENTIFIER = "[a-zA-Z0-9_-]+";
}

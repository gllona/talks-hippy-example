package org.app;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.TextEncoder;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.Normalizer;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Utils {

    private static final int DELETE_TMP_FILE_AFTER_SECONDS = 30;

    private static Logger logger = LogManager.getLogger(Utils.class);

    public static File fileFrom(TalksConfiguration talksConfig, String filename) {
        if (filename == null) {
            return null;
        }

        ClassLoader classLoader = ClassLoader.getSystemClassLoader();

        //URL url = classLoader.getResource(filename);
        //if (url == null) {
        //    logger.warn("Can not load resource " + filename);
        //    return null;
        //}
        //return new File(url.getFile());

        // temporary solution to read files contained in JAR files
        try (InputStream stream = classLoader.getResourceAsStream(filename)) {
            File tmpFile = File.createTempFile("resource_file_", ".file");
            FileUtils.copyInputStreamToFile(stream, tmpFile);
            talksConfig.getFileSystem().deleteAfter(tmpFile, DELETE_TMP_FILE_AFTER_SECONDS, TimeUnit.SECONDS);
            return tmpFile;
        } catch (IOException e) {
            logger.warn("Can not load resource " + filename + " into InputStream");
            return null;
        }
    }

    public static String readResourceFileContents(String resourceFilename) {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(resourceFilename);
        if (is == null) {
            return null;
        }
        InputStreamReader streamReader = new InputStreamReader(is, StandardCharsets.UTF_8);
        BufferedReader reader = new BufferedReader(streamReader);
        StringJoiner stringJoiner = new StringJoiner("\n");
        try {
            for (String line; (line = reader.readLine()) != null; ) {
                stringJoiner.add(line);
            }
        } catch (IOException e) {
            return null;
        }
        return stringJoiner.toString();
    }

    public static String normalizeInternationalCharacters(String text) {
        if (text == null) {
            return null;
        }

        text = Normalizer.normalize(text, Normalizer.Form.NFD);
        text = text.replaceAll("\\p{M}", "");
        return text;
    }

    public static String normalizeAndEncode(String text) {
        return TextEncoder.normalizeAndEncode(text);
    }

    public static String unencode(String text) {
        return TextEncoder.unencode(text);
    }

    public static String generateApiKey() {
        return UUID.randomUUID().toString();
    }

    public static boolean isISOLanguageCode(String languageCode) {
        return Arrays.asList(Locale.getISOLanguages()).contains(languageCode);
    }

    public static Integer intFrom(String text) {
        try {
            return text == null ? null : Integer.parseInt(text);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static <T> T nullIfEquals(T value, Object compareToValue) {
        return Objects.equals(value, compareToValue) ? null : value;
    }
}

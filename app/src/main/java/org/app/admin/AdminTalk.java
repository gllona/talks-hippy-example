package org.app.admin;

import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.Optional;

public class AdminTalk extends Talk {

    private UserService userService = UserService.getInstance(talksConfig);

    public AdminTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("Main");
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of(className(this), "admin.AdminTalk::admin", InputTrigger.ofString("/admin")));
    }

    public TalkResponse admin(TalkRequest request) {
        if (! userService.userCanAuthorize(request.getChatId())) {
            return unauthorizedResponse(request);
        }

        return TalkResponse.ofText(
                className(this), request.getChatId(),
            "Select one or go back to /start"
           ).withButton("admin.AuthTalk::authorizations", "Authorizations")
            .withButton("admin.SettingsTalk::settings", "Settings");
    }

    private TalkResponse unauthorizedResponse(TalkRequest request) {
        return TalkResponse.ofText(
            className(this), request.getChatId(),
            "Not authorized."
           ).withPreserveHandlers();
    }
}

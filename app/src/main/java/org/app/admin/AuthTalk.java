package org.app.admin;

import org.app.AppSetting;
import org.app.Triggers;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

import java.util.Optional;
import java.util.stream.Collectors;

public class AuthTalk extends Talk {

    private UserService userService = UserService.getInstance(talksConfig);

    public AuthTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("Main");
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.empty();
    }

    public TalkResponse firstTimeCode(TalkRequest request) {
        int number = Integer.parseInt(request.getText());

        if (number == getMagicCode()) {
            userService.firstTimeMagicCodeGot(request);

            return TalkResponse.ofText(className(this), request.getChatId(), "Got it! Now tap on /start");
        }

        return TalkResponse.ofText(className(this), request.getChatId(), "Retry.")
            .withRegex("admin.AuthTalk::firstTimeCode", Triggers.FOUR_DIGITS);
    }

    public TalkResponse authorizations(TalkRequest request) {
        if (! userService.userCanAuthorize(request.getChatId())) {
            return unauthorizedResponse(request);
        }

        return TalkResponse.ofText(
                className(this), request.getChatId(),
            "Grant or revoke permits:"
           ).withButton("admin.AuthTalk::list", "List")
            .withButton("admin.AuthTalk::grant", "Grant")
            .withButton("admin.AuthTalk::revoke", "Revoke")
            .withButton("admin.AdminTalk::admin", "Go back");
    }

    public TalkResponse list(TalkRequest request) {
        if (! userService.userCanAuthorize(request.getChatId())) {
            return unauthorizedResponse(request);
        }

        return TalkResponse.ofText(
            className(this), request.getChatId(),
            userService.findAllPrivilegedUsers().stream().map(User::prettyToString).collect(Collectors.joining("\n\n"))
           ).withButton("admin.AuthTalk::authorizations", "Continue");
    }

    public TalkResponse grant(TalkRequest request) {
        if (! userService.userCanAuthorize(request.getChatId())) {
            return unauthorizedResponse(request);
        }

        getContext().set(request.getChatId(), "ADMIN_GRANTREVOKE", "GRANT");

        return grantRevokeStart(request);
    }

    public TalkResponse revoke(TalkRequest request) {
        if (! userService.userCanAuthorize(request.getChatId())) {
            return unauthorizedResponse(request);
        }

        getContext().set(request.getChatId(), "ADMIN_GRANTREVOKE", "REVOKE");

        return grantRevokeStart(request);
    }

    private TalkResponse grantRevokeStart(TalkRequest request) {
        return TalkResponse.ofText(
            className(this), request.getChatId(),
            "Enter username:"
           ).withRegex("admin.AuthTalk::grantRevokeProcessUsername", Triggers.NOT_A_COMMAND);
    }

    public TalkResponse grantRevokeProcessUsername(TalkRequest request) {
        if (! userService.userCanAuthorize(request.getChatId())) {
            return unauthorizedResponse(request);
        }

        String username = request.getText();
        if (! username.startsWith("@")) {
            return TalkResponse.ofHtml(
                className(this), request.getChatId(),
                "Username should be like <code>@username</code> for Telegram and like <code>@username:example.com</code> for other platforms. Please retry."
            ).withPreserveHandlers();
        }

        getContext().set(request.getChatId(), "ADMIN_GRANT_USERNAME", username);

        TalkResponse response = TalkResponse.ofText(
            className(this), request.getChatId(),
            "Select Role:"
        );
        for (Role role : Role.privileged()) {
            response.withButton("admin.AuthTalk::grantRevokeProcessRole", role.name());
        }
        return response;
    }

    public TalkResponse grantRevokeProcessRole(TalkRequest request) {
        if (! userService.userCanAuthorize(request.getChatId())) {
            return unauthorizedResponse(request);
        }

        Role role = Role.from(request.getText());
        if (role == null) {
            return invalidEntryResponse(request);
        }

        getContext().set(request.getChatId(), "ADMIN_GRANT_ROLE", role.toString());

        TalkResponse response = TalkResponse.ofText(
            className(this), request.getChatId(),
            "Select Scope:"
           ).withButton("admin.AuthTalk::grantRevokeProcessScope", "All");
        for (String scope : Scope.getAll()) {
            response.withButton("admin.AuthTalk::grantRevokeProcessScope", scope);
        }
        return response;
    }

    public TalkResponse grantRevokeProcessScope(TalkRequest request) {
        if (! userService.userCanAuthorize(request.getChatId())) {
            return unauthorizedResponse(request);
        }

        String scope = "All".equals(request.getText()) ?
            null :
            request.getText();

        if (! Scope.isValid(scope)) {
            return invalidEntryResponse(request);
        }

        String operation = getContext().getString(request.getChatId(), "ADMIN_GRANTREVOKE");

        if ("GRANT".equals(operation)) {
            userService.save(
                getContext().getString(request.getChatId(), "ADMIN_GRANT_USERNAME"),
                Role.valueOf(getContext().getString(request.getChatId(), "ADMIN_GRANT_ROLE")),
                scope
            );
        } else if ("REVOKE".equals(operation)) {
            userService.delete(
                getContext().getString(request.getChatId(), "ADMIN_GRANT_USERNAME"),
                Role.valueOf(getContext().getString(request.getChatId(), "ADMIN_GRANT_ROLE")),
                scope
            );
        } else {
            throw new RuntimeException("invalid auth operation " + operation);
        }

        return TalkResponse.ofText(
            className(this), request.getChatId(),
            "Operation completed."
           ).withButton("admin.AuthTalk::authorizations", "Continue");
    }

    private TalkResponse invalidEntryResponse(TalkRequest request) {
        return TalkResponse.ofText(
            className(this), request.getChatId(),
            "Invalid entry."
        ).withPreserveHandlers();
    }

    private TalkResponse unauthorizedResponse(TalkRequest request) {
        return TalkResponse.ofText(
            className(this), request.getChatId(),
            "Not authorized."
        ).withPreserveHandlers();
    }

    private int getMagicCode() {
        return AppSetting.getInteger(AppSetting.MAGIC_CODE);
    }
}

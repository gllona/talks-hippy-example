package org.app.admin;

import org.app.Area;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Scope {

    public static List<String> getAll() {
        return Stream.of(Area.values())
            .map(Enum::name)
            .collect(Collectors.toList());
    }

    public static boolean isValid(String scope) {
        if (scope == null) {
            return true;
        }
        try {
            Area.valueOf(scope);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static String from(String scope) {
        return Optional.ofNullable(Area.from(scope))
            .map(Enum::name)
            .orElse(null);
    }
}

package org.app.admin;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.logicas.librerias.copersistance.entities.SingleKeyEntity;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
public class User extends SingleKeyEntity {

    public static final String FIELD_ID = "id";
    public static final String FIELD_CHAT_ID = "chat_id";
    public static final String FIELD_USERNAME = "username";
    public static final String FIELD_PHONE_NUMBER = "phone_number";
    public static final String FIELD_EMAIL = "email";
    public static final String FIELD_ROLE = "role";
    public static final String FIELD_SCOPE = "scope";
    public static final String FIELD_CLIENT_ID = "client_id";
    public static final String FIELD_VISITS = "visits";

    public static final String[] FIELDS = {
        FIELD_ID,
        FIELD_CHAT_ID,
        FIELD_USERNAME,
        FIELD_PHONE_NUMBER,
        FIELD_EMAIL,
        FIELD_ROLE,
        FIELD_SCOPE,
        FIELD_CLIENT_ID,
        FIELD_VISITS
    };

    private static final String[] KEYS = {
        FIELD_ID
    };

    private Long id;
    private String chatId;
    private String username;
    private String phoneNumber;
    private String email;
    private String role;
    private String scope;
    private Long clientId;
    private Integer visits;

    @Override
    public Map<String, Object> parameters() {
        Map<String, Object> params = new HashMap<>();

        conditionalAddToParams(id, FIELD_ID, params);
        conditionalAddToParams(chatId, FIELD_CHAT_ID, params);
        conditionalAddToParams(username, FIELD_USERNAME, params);
        conditionalAddToParams(phoneNumber, FIELD_PHONE_NUMBER, params);
        conditionalAddToParams(email, FIELD_EMAIL, params);
        conditionalAddToParams(role, FIELD_ROLE, params);
        conditionalAddToParams(scope, FIELD_SCOPE, params);
        conditionalAddToParams(clientId == null ? NULL_TAG : clientId, FIELD_CLIENT_ID, params);
        conditionalAddToParams(visits, FIELD_VISITS, params);

        return params;
    }

    @Override
    public String[] getKeyColumns() {
        return KEYS;
    }

    public Role getRole() {
        return Role.valueOf(role);
    }

    public void setRole(Role role) {
        this.role = role.name();
    }

    public boolean isOperator() {
        return getRole() == Role.OPERATOR;
    }

    public boolean hasScope(String scope) {
        return Objects.equals(scope, this.scope);
    }

    public String prettyToString() {
        return "Telegram username = " + (username == null ? "(not defined)" : username) +
            "\n  email = " + (email == null ? "(not defined)" : email) +
            "\n  role = " + role +
            "\n  scope = " + (scope == null ? "(all)" : scope);
    }
}

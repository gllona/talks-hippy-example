package org.app.admin;

import org.app.PhoneNumbers;
import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.repositories.QueryBuilder;
import org.logicas.librerias.copersistance.repositories.SingleKeyEntityRepository;
import org.logicas.librerias.talks.engine.PersistentTalkContextRepository;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class UserRepository extends SingleKeyEntityRepository<User> {

    private static UserRepository instance;

    private Dao dao;
    private final PersistentTalkContextRepository contextRepository;

    private UserRepository(Dao dao) {
        this.dao = dao;
        contextRepository = PersistentTalkContextRepository.getInstance(dao);
    }

    public static synchronized UserRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new UserRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "users";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return User.FIELDS;
    }

    @Override
    public String idFieldName() {
        return User.FIELD_ID;
    }

    public boolean hasAdmins() {
        return countAll("role = 'ADMIN'") > 0;
    }

    public List<User> readByChatId(String chatId) {
        return readByField(User.FIELD_CHAT_ID, chatId, false, false);
    }

    public List<User> readByUsername(String username) {
        return readByField(User.FIELD_USERNAME, username, false, false);
    }

    public List<User> readByClientId(Long clientId) {
        return readByCondition(String.format("%s = %s", User.FIELD_CLIENT_ID, clientId));
    }

    public List<User> readAllPrivileged() {
        return readByCondition("role <> 'USER'");
    }

    public boolean isPrivileged(String chatId) {
        return readAllPrivileged().stream()
            .filter(u -> u.getChatId() != null)
            .anyMatch(u -> Objects.equals(u.getChatId(), chatId));
    }

    public List<User> readByUsernameAndRoleAndScope(String username, Role role, String scope) {
        String condition = scope != null ?
            "username = '" + QueryBuilder.escapeSql(username) + "'" +
                " AND role = '" + role.name() + "'" +
                " AND scope = '" + scope + "'" :
            "username = '" + QueryBuilder.escapeSql(username) + "'" +
                " AND role = '" + role.name() + "'" +
                " AND scope IS NULL" ;
        return readByCondition(condition);
    }

    public List<User> readByRoleAndScope(Role role, String scope) {
        String condition = scope != null ?
            "role = '" + role.name() + "' AND scope = '" + scope + "'" :
            "role = '" + role.name() + "' AND scope IS NULL" ;
        return readByCondition(condition);
    }

    public List<User> readByRoleAndScopeWithTgChatIdSet(Role role, String scope) {
        return readByRoleAndScope(role, scope).stream()
            .filter(u -> u.getChatId() != null)
            .collect(Collectors.toList());
    }

    public List<User> readByPhoneNumberWithEmptyChatId(String phoneNumber) {
        String phone = PhoneNumbers.normalized(phoneNumber);
        if (phone == null) {
            return List.of();
        }
        return readByCondition(String.format("%s = '%s' AND %s IS NULL",
                User.FIELD_PHONE_NUMBER, QueryBuilder.escapeSql(phoneNumber),
                User.FIELD_CHAT_ID
            ));
    }

    public List<User> readUsersWithPhoneNumbersAndEmptyChatId() {
        return readByCondition(String.format("%s IS NOT NULL AND %s IS NULL", User.FIELD_PHONE_NUMBER, User.FIELD_CHAT_ID));
    }

    public List<String> readRelatedSessionIds(String chatIdOrPhoneNumber) {
        String phoneNumber = PhoneNumbers.normalized(chatIdOrPhoneNumber);
        if (phoneNumber == null) {
            return contextRepository.readRelatedSessionIds(chatIdOrPhoneNumber);
        } else {
            return readByPhoneNumberWithEmptyChatId(phoneNumber).stream()
                .map(User::getChatId)
                .filter(Objects::nonNull)
                .distinct()
                .flatMap(sessionId -> contextRepository.readRelatedSessionIds(sessionId).stream())
                .distinct()
                .toList();
        }
    }
}

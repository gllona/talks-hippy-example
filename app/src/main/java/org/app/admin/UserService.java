package org.app.admin;

import org.app.PhoneNumbers;
import org.app.Triggers;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserService {

    private static UserService instance;

    private TalksConfiguration talksConfig;
    private UserRepository repository;

    public static synchronized UserService getInstance(TalksConfiguration talksConfig) {
        if (instance == null) {
            instance = new UserService(talksConfig);
        }
        return instance;
    }

    public static UserService getInstance() {
        if (instance == null) {
            throw new RuntimeException("Auth not initialized");
        }
        return instance;
    }

    private UserService(TalksConfiguration talksConfig) {
        this.talksConfig = talksConfig;
        this.repository = UserRepository.getInstance(talksConfig.getDao());
    }

    public Optional<TalkResponse> firstTimeRoute(String chatId, String callerTalkClassName) {
        if (repository.hasAdmins()) {
            return Optional.empty();
        }

        return Optional.of(
            TalkResponse.ofText(callerTalkClassName, chatId, "Welcome")
                .withRegex("admin.AuthTalk::firstTimeCode", Triggers.FOUR_DIGITS)
        );
    }

    public void firstTimeMagicCodeGot(TalkRequest request) {
        User user = new User();
        user.setChatId(request.getChatId());
        user.setUsername(request.getUsername());
        user.setRole(Role.ADMIN);
        repository.save(user);
    }

    public boolean userCanAuthorize(String chatId) {
        return userHasRole(chatId, Role.ADMIN, Optional.empty());
    }

    public boolean userCanModifySettings(String chatId) {
        return userHasRole(chatId, Role.ADMIN, Optional.empty());
    }

    public boolean userHasRole(String chatId, Role role, Optional<String> scope) {
        List<User> usersWithRoles = repository.readByChatId(chatId);

        Optional<User> userRole = usersWithRoles.stream()
            .filter(u -> u.getRole() == role)
            .filter(u -> scope.isEmpty() || scope.get().equals(u.getScope()))
            .findFirst();

        return userRole.isPresent();
    }

    public User findOperatorByChatId(String chatId, String scope) {
        return repository.readByChatId(chatId).stream()
            .filter(User::isOperator)
            .filter(user -> user.hasScope(scope))
            .findFirst()
            .orElse(null);
    }

    public User findOperatorByUsername(String username, String scope) {
        return repository.readByUsername(username).stream()
            .filter(User::isOperator)
            .filter(user -> user.hasScope(scope))
            .findFirst()
            .orElse(null);
    }

    public User findOperatorByClientId(Long clientId, String scope) {
        return repository.readByClientId(clientId).stream()
            .filter(User::isOperator)
            .filter(user -> user.hasScope(scope))
            .findFirst()
            .orElse(null);
    }

    public List<User> findAllPrivilegedUsers() {
        return repository.readAllPrivileged().stream()
            .filter(u -> u.getRole() != Role.USER)
            .collect(Collectors.toList());
    }

    public List<User> findByPhoneNumber(String phoneNumber) {
        String phone = PhoneNumbers.normalized(phoneNumber);
        if (phone == null) {
            return List.of();
        }
        return Stream.concat(
            repository.readByPhoneNumberWithEmptyChatId(phone).stream(),
            repository.readByChatId(PhoneNumbers.preloadedSessionIdOf(phone)).stream()
        ).toList();
    }

    public void save(String phoneNumber) {
        String phone = PhoneNumbers.normalized(phoneNumber);

        List<User> users = repository.readByPhoneNumberWithEmptyChatId(phoneNumber);
        if (! users.isEmpty()) {
            return;
        }

        User user = new User();
        user.setRole(Role.USER);
        user.setPhoneNumber(phone);
        repository.save(user);
    }

    public void save(String tgUsername, Role role, String scope) {
        List<User> users = repository.readByUsernameAndRoleAndScope(tgUsername, role, scope);
        if (! users.isEmpty()) {
            return;
        }

        User user = new User();
        user.setUsername(tgUsername);
        user.setRole(role);
        user.setScope(scope);
        repository.save(user);
    }

    public void delete(String tgUsername, Role role, String scope) {
        List<User> existingUsers = repository.readByUsernameAndRoleAndScope(tgUsername, role, scope);
        for (User user : existingUsers) {
            repository.delete(user);
        }
    }

    public void fillInUsers(TalkRequest request) {
        if (request.getUsername() == null) {
            return;
        }
        List<User> userRoles = repository.readByField("username", request.getUsername(), false, false);
        for (User userRole : userRoles) {
            userRole.setChatId(request.getChatId());
            repository.save(userRole);
        }
    }

    public void updateClientId(Long userId, Long clientId) {
        User user = repository.readById(userId);
        if (user != null) {
            user.setClientId(clientId);
            repository.save(user);
        }
    }

    public String unifyChatIds(String chatId, String username) {
        String phoneNumber = PhoneNumbers.phoneNumberOf(username);
        if (phoneNumber == null) {
            return null;
        }
        String oldChatId = PhoneNumbers.preloadedSessionIdOf(phoneNumber);
        updateChatId(phoneNumber, chatId);
        return oldChatId;
    }

    public void updateChatId(String oldChatIdOrPhoneNumber, String newChatId) {
        String phoneNumber = PhoneNumbers.normalized(oldChatIdOrPhoneNumber);
        if (phoneNumber == null) {
            for (User user : repository.readByChatId(oldChatIdOrPhoneNumber)) {
                if (! Objects.equals(user.getChatId(), newChatId)) {
                    user.setChatId(newChatId);
                    repository.save(user);
                }
            }
        } else {
            User newChatIdUser = repository.readByChatId(newChatId).stream().findFirst().orElse(null);
            repository.readUsersWithPhoneNumbersAndEmptyChatId().stream()
                .filter(user -> PhoneNumbers.preloadedSessionIdMatchesPhoneNumber(user.getPhoneNumber(), PhoneNumbers.preloadedSessionIdOf(phoneNumber)))
                .forEach(user -> {
                    if (newChatIdUser == null) {
                        user.setChatId(newChatId);
                        repository.save(user);
                    } else if (! Objects.equals(user.getPhoneNumber(), newChatIdUser.getPhoneNumber())) {
                        newChatIdUser.setPhoneNumber(user.getPhoneNumber());
                        repository.save(newChatIdUser);
                        repository.delete(user);
                    } else {
                        repository.delete(user);
                    }
                });
        }
    }

    public boolean userCanCommunicateWithOtherUsers(String chatId) {
        return repository.isPrivileged(chatId);
    }
}

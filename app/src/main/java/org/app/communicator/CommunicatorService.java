package org.app.communicator;

import org.app.Area;
import org.app.admin.Role;
import org.app.admin.User;
import org.app.admin.UserRepository;
import org.logicas.librerias.talks.api.InputAdapterApi;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.config.InputAdapters;
import org.logicas.librerias.talks.engine.TalkResponse;

import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Collections.singletonList;

public class CommunicatorService {

    static final int TG_MESSAGE_LINE_MAX_CHARS = 24;   // 30 in 1080p phones

    private static CommunicatorService instance;

    private final TalksConfiguration talksConfig;
    private final TextWrapperService textWrapperService;
    private final UserRepository userRepository;

    public static synchronized CommunicatorService getInstance(TalksConfiguration talksConfig) {
        if (instance == null) {
            instance = new CommunicatorService(talksConfig);
        }
        return instance;
    }

    public CommunicatorService(TalksConfiguration talksConfig) {
        this.talksConfig = talksConfig;
        this.textWrapperService = TextWrapperService.getInstance(TG_MESSAGE_LINE_MAX_CHARS);
        this.userRepository = UserRepository.getInstance(talksConfig.getDao());
    }

    public void sendToOperatorsUnboxed(Area area,
                                       String header, String message, String footer, String postFooter
    ) {
        sendToOperators(area, Optional.empty(), header, message, footer, postFooter, false);
    }

    public void sendToOperatorsBoxed(Area area,
                                     String header, String message, String footer, String postFooter
    ) {
        sendToOperatorsBoxed(area, Optional.empty(), header, message, footer, postFooter);
    }

    public void sendToOperatorsBoxed(Area area,
                                     Optional<String> exceptThisOperatorChatId,
                                     String header, String message, String footer, String postFooter
    ) {
        sendToOperators(area, exceptThisOperatorChatId, header, message, footer, postFooter, true);
    }

    private void sendToOperators(Area area,
                                 Optional<String> exceptThisOperatorChatId,
                                 String header, String message, String footer, String postFooter,
                                 boolean boxed
    ) {
        List<String> operators = getOperatorsChatIds(area, exceptThisOperatorChatId);

        sendTo(operators, header, message, footer, postFooter, boxed);
    }

    public void sendToOperators(Area area, File file) {
        sendToOperators(area, Optional.empty(), file);
    }

    public void sendToOperators(Area area, Optional<String> exceptThisOperatorChatId, File file) {
        List<String> operators = getOperatorsChatIds(area, exceptThisOperatorChatId);

        sendTo(operators, file);
    }

    private List<String> getOperatorsChatIds(Area area, Optional<String> exceptThisOperatorChatId) {
        List<User> allOperators = userRepository.readByRoleAndScopeWithTgChatIdSet(Role.OPERATOR, area == null ? null : area.name());
        List<String> operators = allOperators.stream()
            .filter(operator -> exceptThisOperatorChatId.isEmpty() || ! operator.getChatId().equals(exceptThisOperatorChatId.get()))
            .map(User::getChatId)
            .collect(Collectors.toList());
        return operators;
    }

    public void sendToUser(String targetChatId,
                           String header, String message, String footer, String postFooter,
                           boolean boxed
    ) {
        sendTo(singletonList(targetChatId), header, message, footer, postFooter, boxed);
    }

    private void sendTo(List<String> chatIds,
                        String header, String message, String footer, String postFooter,
                        boolean boxed
    ) {
        String fullMessage = Stream.of(
            Optional.ofNullable(header),
            Optional.of(""),
            Optional.ofNullable(message),
            Optional.of(""),
            Optional.ofNullable(footer)
        ).filter(Optional::isPresent)
         .map(Optional::get)
         .collect(Collectors.joining("\n"));
        List<String> fullMessageParts = buildMessageParts(fullMessage, boxed);

        for (String chatId : chatIds) {
            for (String part : fullMessageParts) {
                send(
                    TalkResponse.ofHtml(
                        null,
                        chatId,
                        part
                    ).withPreserveHandlers()
                );
            }
            if (postFooter != null) {
                send(
                    TalkResponse.ofText(
                        null,
                        chatId,
                        postFooter
                    ).withPreserveHandlers()
                );
            }
        }
    }

    private void sendTo(List<String> chatIds, File file) {
        for (String chatId : chatIds) {
            send(
                TalkResponse.ofDocument(
                    null,
                    chatId,
                    file
                ).withPreserveHandlers()
            );
        }
    }

    public void sendToAdmins(String message) {
        List<User> admins = userRepository.readByRoleAndScopeWithTgChatIdSet(Role.ADMIN, null);
        List<String> boxedMessageParts = buildMessageParts(message, true);

        for (User admin : admins) {
            for (String part : boxedMessageParts) {
                send(
                    TalkResponse.ofHtml(
                        null,
                        admin.getChatId(),
                        part
                    ).withPreserveHandlers()
                );
            }
        }
    }

    private List<String> buildMessageParts(String fullMessage, boolean boxed) {
        if (boxed) {
            return textWrapperService.boxAndSplit(fullMessage);
        } else {
            return textWrapperService.splitToTelegram(fullMessage, true);
        }
    }

    private void send(TalkResponse talkResponse) {
        InputAdapters inputAdapterType = InputAdapters.forTalkResponse(talkResponse);
        InputAdapterApi inputAdapter = talksConfig.getInputAdaptersManager().get(inputAdapterType);
        inputAdapter.sendResponse(talkResponse);
    }
}

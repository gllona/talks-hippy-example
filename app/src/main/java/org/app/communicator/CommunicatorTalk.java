package org.app.communicator;

import org.app.Area;
import org.app.Triggers;
import org.app.admin.UserService;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.Optional;

public class CommunicatorTalk extends Talk {

    private static final String OPERATOR = "Operator";
    private static final String OPERATORS = "Operators";
    private static final String EXIT_CLASS_AND_METHOD = "main.MainTalk::main";

    private UserService userService = UserService.getInstance(talksConfig);
    private CommunicatorService communicatorService = CommunicatorService.getInstance(talksConfig);

    public CommunicatorTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("Main");
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.of(
            InputHandler.of(className(this), "communicator.CommunicatorTalk::write",
                InputTrigger.ofRegex("/reply(___)?([A-Za-z0-9][A-Za-z0-9_]*)?"))
        );
    }

    public TalkResponse write(TalkRequest request) {
        String chatIdAndUserId = request.getMatch().getRegexMatches().get(2);

        if (chatIdAndUserId != null) { // if the command is /reply___CHATID or /reply___CHATID___USERID
            String targetChatId;
            String targetTgUserId;
            int pos = chatIdAndUserId.indexOf("___");
            if (pos == -1) { // if the command is /reply___CHATID
                targetChatId = unencodeChatId(chatIdAndUserId);
                targetTgUserId = null;
            } else { // if the command is /reply___CHATID___USERID
                targetChatId = unencodeChatId(chatIdAndUserId.substring(0, pos));
                targetTgUserId = unencodeUsername(chatIdAndUserId.substring(pos + 3));
            }

            if (userService.userCanCommunicateWithOtherUsers(request.getChatId())) {
                return writeToUser(request, targetChatId, targetTgUserId);
            } else {
                return unauthorizedOperation(request);
            }
        } else { // if the command is /reply
            return writeToOperators(request);
        }
    }

    private TalkResponse writeToUser(TalkRequest request, String targetChatId, String targetTgUserId) {
        getContext().set(request.getChatId(), "TARGET_CHAT_ID", targetChatId);
        getContext().set(request.getChatId(), "TARGET_TG_USER_ID", targetTgUserId);

        return TalkResponse.ofText(
            className(this), request.getChatId(),
            "Enter your response to the user in a single message:"
        ).withRegex("communicator.CommunicatorTalk::doWriteToUser", Triggers.NOT_A_COMMAND);
    }

    public TalkResponse doWriteToUser(TalkRequest request) {
        Area area = Area.from(
            getContext().getString(request.getChatId(), "AREA")
        );
        String targetChatId = getContext().getString(request.getChatId(), "TARGET_CHAT_ID");
        String targetTgUserId = getContext().getString(request.getChatId(), "TARGET_TG_USER_ID");
        String targetScope = getContext().getString(targetChatId, "AREA");
        String operatorTgUserId = request.getUsername();

        communicatorService.sendToOperatorsBoxed(area, Optional.of(request.getChatId()),
            OPERATORS.toUpperCase() + " WROTE TO USER"
                + (targetScope == null ? "" : " (" + targetScope + ")"),
            request.getText(),
            "END OF MESSAGE",
            "To reply: /reply___" + encodeChatId(targetChatId) + (targetTgUserId == null ? "" : "___" + encodeUsername(targetTgUserId))
                + (targetTgUserId == null ? "" : " or " + targetTgUserId)
                + ". Or talk to the " + OPERATOR + ": " + operatorTgUserId
            );

        communicatorService.sendToUser(
            targetChatId,
            "MESSAGE FROM " + OPERATORS.toUpperCase(),
            request.getText(),
            "END OF MESSAGE",
            "To reply to " + OPERATORS + " please tap on /reply",
            true
        );

        return TalkResponse.ofText(
            className(this), request.getChatId(),
            "The response has been sent to the user\n"
                + "\n"
                + "You can send another reply with /reply___" + targetChatId
        );
    }

    public TalkResponse writeToOperators(TalkRequest request) {
        String scopeStr = getContext().getString(request.getChatId(), "AREA");
        Area area = Area.from(scopeStr);

        return TalkResponse.ofText(
                className(this), request.getChatId(),
            "📥 You can send us any questions or comments here.\n"
                + "\n"
                + "Please write a single message with your question. Or go back to the main /menu"
        ).withString(EXIT_CLASS_AND_METHOD, "/menu")
         .withRegex("communicator.CommunicatorTalk::doWriteToOperators", Triggers.NOT_A_COMMAND);
    }

    public TalkResponse doWriteToOperators(TalkRequest request) {
        String scopeStr = getContext().getString(request.getChatId(), "AREA");
        Area area = Area.from(scopeStr);

        String tgUsername = request.getUsername();
        String tgFullName = request.getUserFullName();
        if (tgFullName == null || tgFullName.isBlank()) {
            tgFullName = "N/A";
        }

        communicatorService.sendToOperatorsBoxed(
            area,
            "MESSAGE FROM USER\n"
                + tgFullName
                + (scopeStr == null ? "" : " (" + scopeStr + ")")
                + "\n"
                + "Has submitted the following comment or question:",
            request.getText(),
            "END OF MESSAGE",
            "To reply: /reply___" + encodeChatId(request.getChatId()) + (request.getUsername() == null ? "" : "___" + encodeUsername(request.getUsername()))
                + (tgUsername == null ? "" : " or " + tgUsername)
        );

        return TalkResponse.ofText(
            className(this), request.getChatId(),
            "Thanks. If it's a question, one of us will contact you soon.\n"
                + "\n"
                + "To send us another message tap on /contact\n"
                + "\n"
                + "* Back to the main /menu"
        ).withString(EXIT_CLASS_AND_METHOD, "/menu");
    }

    private TalkResponse unauthorizedOperation(TalkRequest request) {
        return TalkResponse.ofText(
            className(this), request.getChatId(),
            "You do not have privileges for this operation\n"
                + "\n"
                + "Back to the /menu"
        ).withString(EXIT_CLASS_AND_METHOD, "/menu");
    }

    private String encodeChatId(String chatId) {
        if (chatId == null) {
            return null;
        }

        if (chatId.startsWith("!")) { // Matrix case: !ABCDabcd1234:server.com -> ABCDabcd1234__server_com
            int pos = chatId.indexOf(":");
            String roomPart = chatId.substring(1, pos);
            String serverPart = chatId.substring(pos + 1);
            serverPart = serverPart.replaceAll("\\.", "_");
            return roomPart + "__" + serverPart;
        } else { // Telegram case: 1234567890 -> 1234567890
            return chatId;
        }
    }

    private String unencodeChatId(String encodedChatId) {
        if (encodedChatId == null) {
            return null;
        }

        int pos = encodedChatId.indexOf("__");
        if (pos == -1) { // Telegram case: 1234567890 -> 1234567890
            return encodedChatId;
        } else { // Matrix case: ABCDabcd1234__server_com -> !ABCDabcd1234:server.com
            String roomPart = encodedChatId.substring(0, pos);
            String serverPart = encodedChatId.substring(pos + 2);
            serverPart = serverPart.replaceAll("_", ".");
            return "!" + roomPart + ":" + serverPart;
        }
    }

    private String encodeUsername(String username) {
        if (username == null) {
            return null;
        }

        int pos = username.indexOf(":");
        if (pos == -1) { // Telegram case: @user_name -> @user_name
            return username.substring(1);
        } else { // Matrix case: @user_name:server.com -> user_name__server_com
            String usernamePart = username.substring(1, pos);
            String serverPart = username.substring(pos + 1);
            serverPart = serverPart.replaceAll("\\.", "_");
            return usernamePart + "__" + serverPart;
        }
    }

    private String unencodeUsername(String encodedUsername) {
        if (encodedUsername == null) {
            return null;
        }

        int pos = encodedUsername.indexOf("__");
        if (pos == -1) { // Telegram case: username -> @username
            return "@" + encodedUsername;
        } else { // Matrix case: username__server_com -> @username:server.com
            String usernamePart = encodedUsername.substring(0, pos);
            String serverPart = encodedUsername.substring(pos + 2);
            serverPart = serverPart.replaceAll("_", ".");
            return "@" + usernamePart + ":" + serverPart;
        }
    }
}

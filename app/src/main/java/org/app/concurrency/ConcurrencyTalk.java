package org.app.concurrency;

import org.app.main.Main;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.Optional;

public class ConcurrencyTalk extends Talk {

    public ConcurrencyTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext(Main.class);
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of(className(this), "concurrency.ConcurrencyTalk::main", InputTrigger.ofString("/concurrency")));
    }

    public TalkResponse main(TalkRequest request) {
        context.set(request.getChatId(), "CONCURRENCY_TEST_VALUE", 0);

        return TalkResponse.ofText(
            className(this), request.getChatId(),
            "Shows how the framework manages context for concurrent messages sent from the same user (e.g. two client apps logged to the same user account)" +
                "\n" +
                "\nThis test makes sense only if you are using webhooks, e.g. Matrix client or Telegram with webhooks. Telegram uses polling by default." +
                "\n" +
                "\n- Log in to a secondary session with the same user account" +
                "\n- Enter \"Continue\" at the same time in both sessions" +
                "\n" +
                "\nEach bot session will read a context variable that was already initialized to 0, add 15 to it, wait 5 seconds, display the new value, store it in the context and exit" +
                "\n" +
                "\nYou should see one of the session show the value 15 after 5 seconds, and the other session show the value 30 after 10 seconds."
        ).withButton("concurrency.ConcurrencyTalk::test", "Continue");
    }

    public TalkResponse test(TalkRequest request) {
        Integer value = context.getInt(request.getChatId(), "CONCURRENCY_TEST_VALUE");

        try {
            Thread.sleep(5 * 1000);
        } catch (InterruptedException e) {
            return interrupted(request);
        }

        value += 15;
        context.set(request.getChatId(), "CONCURRENCY_TEST_VALUE", value);

        return TalkResponse.ofText(
                className(this), request.getChatId(),
            "Wait period completed. Value was updated to " + value
        ).withButton("concurrency.ConcurrencyTalk::test", "Continue")
         .withButton("main.MainTalk::tests", "Back to tests");
    }

    private TalkResponse interrupted(TalkRequest request) {
        return TalkResponse.ofText(
            className(this), request.getChatId(),
            "The thread was unexpectedly interrupted during the wait period."
        ).withButton("main.MainTalk::tests", "Back to tests");
    }
}

package org.app.controller;

import org.app.AppSetting;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.health.HealthService;
import spark.Response;

import static spark.Spark.get;

public class HealthController {

    private static HealthController instance = null;

    private final HealthService healthService;
    private final TalksConfiguration talksConfig;

    public static synchronized HealthController getInstance(TalksConfiguration configuration) {
        if (instance == null) {
            instance = new HealthController(configuration);
        }
        return instance;
    }

    private HealthController(TalksConfiguration talksConfig) {
        this.talksConfig = talksConfig;
        this.healthService = HealthService.getInstance(talksConfig);
        registerEndpoints();
    }

    private void registerEndpoints() {
        registerHealthEndpoint();
    }

    private void registerHealthEndpoint() {
        String endpoint = AppSetting.get(AppSetting.HEALTH_ENDPOINT);

        get(endpoint, (restRequest, restResponse) -> {
            HealthResponse healthResponse;

            if (! RestUtilsService.isAuthorizedByBearer(restRequest, AppSetting.HEALTH_ENDPOINTS_API_KEY)) {
                restResponse.status(401);
                healthResponse = HealthResponse.error(RestUtilsService.UNAUTHORIZED);
                return buildResponse(restResponse, healthResponse);
            }

            try {
                healthService.check();
                healthResponse = HealthResponse.ok();
            } catch (Exception e) {
                restResponse.status(500);
                healthResponse = HealthResponse.error(e.getMessage());
            }

            return buildResponse(restResponse, healthResponse);
        });
    }

    private static String buildResponse(Response restResponse, HealthResponse healthResponse) {
        String healthResponseJson = RestUtilsService.gson().toJson(healthResponse);
        restResponse.type(RestUtilsService.APPLICATION_JSON);
        return healthResponseJson;
    }
}

package org.app.controller;

import lombok.Value;

@Value
public class HealthResponse {
    String status;
    String message;

    public static HealthResponse ok() {
        return new HealthResponse("OK", null);
    }

    public static HealthResponse error(String message) {
        return new HealthResponse("KO", message);
    }
}

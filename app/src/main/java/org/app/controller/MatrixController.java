package org.app.controller;

import org.apache.commons.text.StringEscapeUtils;
import org.app.AppSetting;
import org.app.tags.TagService;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.matrix.MatrixConfirmMessagesRequest;
import org.logicas.librerias.talks.matrix.MatrixReceiveMessageRequest;
import org.logicas.librerias.talks.matrix.MatrixResponse;
import org.logicas.librerias.talks.matrix.MatrixService;
import spark.Response;

import static spark.Spark.get;
import static spark.Spark.post;

public class MatrixController {

    private static MatrixController instance = null;

    private final TalksConfiguration talksConfig;
    private final MatrixService matrixService;
    private final TagService tagService;

    public static synchronized MatrixController getInstance(TalksConfiguration configuration) {
        if (instance == null) {
            instance = new MatrixController(configuration);
        }
        return instance;
    }

    public MatrixController(TalksConfiguration talksConfig) {
        this.talksConfig = talksConfig;
        this.matrixService = MatrixService.getInstance(talksConfig);
        this.tagService = TagService.getInstance(talksConfig);
        registerEndpoints();
    }

    private void registerEndpoints() {
        registerReceiveMessageEndpoint();
        registerMessagePollingEndpoints();
        registerTagRoomEndpoint();
    }

    private void registerReceiveMessageEndpoint() {
        String endpoint = AppSetting.get(AppSetting.MATRIX_ENDPOINT_RECEIVE_MESSAGE);

        post(endpoint, (restRequest, restResponse) -> {
            MatrixResponse matrixResponse;

            if (! RestUtilsService.isAuthorizedByBearer(restRequest, AppSetting.MATRIX_ENDPOINTS_API_KEY)) {
                restResponse.status(401);
                matrixResponse = MatrixResponse.error(RestUtilsService.UNAUTHORIZED);
                return buildResponse(restResponse, matrixResponse);
            }

            MatrixReceiveMessageRequest matrixRequest = null;
            try {
                matrixRequest = RestUtilsService.gson().fromJson(cleanJson(restRequest.body()), MatrixReceiveMessageRequest.class);
            } catch (Exception ignored) {
            }
            if (matrixRequest != null && ! matrixRequest.shouldProcess()) {
                matrixResponse = MatrixResponse.accepted();
                return buildResponse(restResponse, matrixResponse);
            } else if (matrixRequest == null || ! matrixRequest.isValid()) {
                restResponse.status(400);
                matrixResponse = MatrixResponse.error(RestUtilsService.INVALID_REQUEST);
                return buildResponse(restResponse, matrixResponse);
            }

            try {
                matrixService.receiveMessage(matrixRequest);
                matrixResponse = MatrixResponse.accepted();
            } catch (Exception e) {
                restResponse.status(500);
                matrixResponse = MatrixResponse.error(e.getMessage());
            }

            return buildResponse(restResponse, matrixResponse);
        });
    }

    private void registerMessagePollingEndpoints() {
        registerGetMessagesEndpoint();
        registerConfirmMessagesEndpoint();
    }

    private void registerGetMessagesEndpoint() {
        String endpoint = AppSetting.get(AppSetting.MATRIX_ENDPOINT_GET_MESSAGES);

        get(endpoint, (restRequest, restResponse) -> {
            MatrixResponse matrixResponse;

            if (! RestUtilsService.isAuthorizedByBearer(restRequest, AppSetting.MATRIX_ENDPOINTS_API_KEY)) {
                restResponse.status(401);
                matrixResponse = MatrixResponse.error(RestUtilsService.UNAUTHORIZED);
                return buildResponse(restResponse, matrixResponse);
            }

            try {
                matrixResponse = matrixService.getMessages();
            } catch (Exception e) {
                restResponse.status(500);
                matrixResponse = MatrixResponse.error(e.getMessage());
            }

            return buildResponse(restResponse, matrixResponse);
        });
    }

    private void registerConfirmMessagesEndpoint() {
        String endpoint = AppSetting.get(AppSetting.MATRIX_ENDPOINT_CONFIRM_MESSAGES);

        post(endpoint, (restRequest, restResponse) -> {
            MatrixResponse matrixResponse;

            if (! RestUtilsService.isAuthorizedByBearer(restRequest, AppSetting.MATRIX_ENDPOINTS_API_KEY)) {
                restResponse.status(401);
                matrixResponse = MatrixResponse.error(RestUtilsService.UNAUTHORIZED);
                return buildResponse(restResponse, matrixResponse);
            }

            MatrixConfirmMessagesRequest matrixRequest = null;
            try {
                matrixRequest = RestUtilsService.gson().fromJson(cleanJson(restRequest.body()), MatrixConfirmMessagesRequest.class);
            } catch (Exception ignored) {
            }
            if (matrixRequest == null || ! matrixRequest.isValid()) {
                restResponse.status(400);
                matrixResponse = MatrixResponse.error(RestUtilsService.INVALID_REQUEST);
                return buildResponse(restResponse, matrixResponse);
            }

            try {
                matrixService.confirmMessages(matrixRequest);
                matrixResponse = MatrixResponse.accepted();
            } catch (Exception e) {
                restResponse.status(500);
                matrixResponse = MatrixResponse.error(e.getMessage());
            }

            return buildResponse(restResponse, matrixResponse);
        });
    }

    private void registerTagRoomEndpoint() {
        String endpoint = AppSetting.get(AppSetting.MATRIX_ENDPOINT_TAG_ROOM);

        post(endpoint, (restRequest, restResponse) -> {
            MatrixResponse matrixResponse;

            if (! RestUtilsService.isAuthorizedByBearer(restRequest, AppSetting.MATRIX_ENDPOINTS_API_KEY)) {
                restResponse.status(401);
                matrixResponse = MatrixResponse.error(RestUtilsService.UNAUTHORIZED);
                return buildResponse(restResponse, matrixResponse);
            }

            MatrixTagRoomRequest matrixRequest = RestUtilsService.gson().fromJson(cleanJson(restRequest.body()), MatrixTagRoomRequest.class);

            if (! matrixRequest.isValid()) {
                restResponse.status(400);
                matrixResponse = MatrixResponse.error(RestUtilsService.INVALID_REQUEST);
                return buildResponse(restResponse, matrixResponse);
            }

            try {
                tagService.set(matrixRequest.getRoomId(), matrixRequest.getTag(), matrixRequest.getValue());
                matrixResponse = MatrixResponse.accepted();
            } catch (Exception e) {
                restResponse.status(500);
                matrixResponse = MatrixResponse.error(e.getMessage());
            }

            return buildResponse(restResponse, matrixResponse);
        });
    }

    private static String buildResponse(Response restResponse, MatrixResponse matrixResponse) {
        String matrixResponseJson = RestUtilsService.gson().toJson(matrixResponse);
        restResponse.type(RestUtilsService.APPLICATION_JSON);
        return matrixResponseJson;
    }

    private String cleanJson(String body) {
        if (body == null) {
            return null;
        }
        String json = body.replace("\\\"", "\"").replaceAll("^\"|\"$", "");
        return StringEscapeUtils.unescapeJava(json);
    }
}

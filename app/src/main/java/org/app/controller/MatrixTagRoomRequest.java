package org.app.controller;

import lombok.Value;

@Value
public class MatrixTagRoomRequest {
    String roomId;
    String tag;
    String value;

    public boolean isValid() {
        return roomId != null && tag != null && value != null;
    }
}

package org.app.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.ToNumberPolicy;
import org.app.AppSetting;
import org.logicas.librerias.talks.api.TalksConfiguration;
import spark.Request;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class RestUtilsService {

    public static final String APPLICATION_JSON = "application/json";
    public static final String TEXT_CSV = "text/csv; charset=UTF-8";
    public static final String TEXT_PLAIN = "text/plain; charset=UTF-8";
    public static final String INVALID_REQUEST = "Invalid request";
    public static final String UNAUTHORIZED = "Unauthorized";
    public static final String NOT_FOUND = "Not found";

    private static final String AUTHORIZATION_HEADER = "Authorization";
    private static final String BEARER = "Bearer";

    private static RestUtilsService instance = null;

    private final TalksConfiguration talksConfig;

    public static synchronized RestUtilsService getInstance(TalksConfiguration talksConfig) {
        if (instance == null) {
            instance = new RestUtilsService(talksConfig);
        }
        return instance;
    }

    private RestUtilsService(TalksConfiguration talksConfig) {
        this.talksConfig = talksConfig;
    }

    private static Gson gson = new GsonBuilder()
        .disableHtmlEscaping()
        //.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
        //.setPrettyPrinting()
        .setObjectToNumberStrategy(ToNumberPolicy.LONG_OR_DOUBLE)
        .serializeNulls()
        .create();

    public static Gson gson() {
        return gson;
    }

    public static Map<String, String> getQueryParamsMap(Request restRequest) {
        return restRequest.queryMap().toMap().entrySet().stream()
            .collect(Collectors.toMap(
                Map.Entry::getKey,
                entry -> Arrays.stream(entry.getValue()).findFirst().orElse("")
            ));
    }

    public static String getQueryParam(Request restRequest, String queryParamKey) {
        return restRequest.queryMap().toMap().entrySet().stream()
            .filter(entry -> Objects.equals(entry.getKey(), queryParamKey))
            .flatMap(entry -> Arrays.stream(entry.getValue()))
            .findFirst()
            .orElse(null);
    }

    public static boolean isAuthorizedByBearer(Request restRequest, AppSetting apiKeySetting) {
        String authorizationHeader = restRequest.headers(AUTHORIZATION_HEADER);

        if (authorizationHeader != null && authorizationHeader.startsWith(BEARER)) {
            String apiKey = authorizationHeader.substring(BEARER.length()).trim();
            if (Objects.equals(AppSetting.get(apiKeySetting), apiKey)) {
                return true;
            }
        }

        return false;
    }
}

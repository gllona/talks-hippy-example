package org.app.eatings;

import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.InputHandler;
import org.logicas.librerias.talks.engine.Talk;
import org.logicas.librerias.talks.engine.TalkRequest;
import org.logicas.librerias.talks.engine.TalkResponse;

import java.util.Optional;

public class EatingsToo extends Talk {

    public EatingsToo(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("Eatings");
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.empty();
    }

    // ADDITIONAL INTERACTIONS

    public TalkResponse history(TalkRequest call) {
        Long plantId = getContext().getLong(call.getChatId(), "plant_id");
        Long animalId = getContext().getLong(call.getChatId(), "animal_id");

        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "At the beginning there were the /plants" +
                "\nMuch much time after there were the /animals" +
                "\nThen dinners began and animals ate plants" +
                "\nIn time some plants ate animals too." +
                (plantId == null ? "" : "\nYour last selected plant ID is " + plantId) +
                (animalId == null ? "" : "\nYour last selected animal ID is " + animalId) +
                "\n/eatings"
        )
        .withString("eatings.Eatings::plants", "/plants")
        .withString("eatings.Eatings::animals", "/animals");
    }
}

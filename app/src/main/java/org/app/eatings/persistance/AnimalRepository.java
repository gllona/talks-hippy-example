package org.app.eatings.persistance;

import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.repositories.QueryBuilder;
import org.logicas.librerias.copersistance.repositories.SingleKeyEntityRepository;

public class AnimalRepository extends SingleKeyEntityRepository<Animal> {

    private static AnimalRepository instance;

    private Dao dao;

    private AnimalRepository(Dao dao) {
        this.dao = dao;
    }

    public static synchronized AnimalRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new AnimalRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "test_animal";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return Animal.FIELDS;
    }

    @Override
    public String idFieldName() {
        return Animal.FIELD_ID;
    }

    public Animal findOneByName(String name) {
        return readOne("name = '" + QueryBuilder.escapeSql(name) + "'");
    }

    public void deleteByName(String name) {
        this.deleteByCondition("name = '" + QueryBuilder.escapeSql(name) + "'");
    }
}

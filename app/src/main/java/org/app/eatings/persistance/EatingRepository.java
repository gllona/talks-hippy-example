package org.app.eatings.persistance;

import java.util.List;
import java.util.Map;
import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.repositories.DualKeyEntityRepository;

public class EatingRepository extends DualKeyEntityRepository<Eating> {

    private static EatingRepository instance;

    private Dao dao;

    private EatingRepository(Dao dao) {
        this.dao = dao;
    }

    public static synchronized EatingRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new EatingRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "test_eatings";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return Eating.FIELDS;
    }

    @Override
    public String id1FieldName() {
        return Eating.FIELD_EATER_ID;
    }

    @Override
    public String id2FieldName() {
        return Eating.FIELD_FOOD_ID;
    }

    public List<Map<String, Object>> findAllWithFullNames() {
        return rawRead(
            "SELECT eatings.*, plant.name AS plant_name, animal.name AS animal_name" +
                " FROM test_eatings AS eatings" +
                " JOIN test_plant AS plant ON eatings.food_id = plant.id" +
                " JOIN test_animal AS animal ON eatings.eater_id = animal.id"
        );
    }
}

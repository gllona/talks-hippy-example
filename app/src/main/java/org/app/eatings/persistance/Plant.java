package org.app.eatings.persistance;

import java.util.HashMap;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.logicas.librerias.copersistance.entities.SingleKeyEntity;

@AllArgsConstructor
@Setter
@Getter
@ToString
public class Plant extends SingleKeyEntity {

    public static final String FIELD_ID = "id";
    public static final String FIELD_AGE = "age";
    public static final String FIELD_NAME = "name";

    public static final String[] FIELDS = {
        FIELD_ID,
        FIELD_AGE,
        FIELD_NAME
    };

    private static final String[] KEYS = {
        FIELD_ID
    };

    private Long id;
    private Integer age;
    private String name;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Map<String, Object> parameters() {
        Map<String, Object> params = new HashMap<>();

        conditionalAddToParams(id, FIELD_ID, params);
        conditionalAddToParams(age, FIELD_AGE, params);
        conditionalAddToParams(name, FIELD_NAME, params);

        return params;
    }

    @Override
    public String[] getKeyColumns() {
        return KEYS;
    }
}

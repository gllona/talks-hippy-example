package org.app.echo;

import org.app.Triggers;
import org.app.main.Main;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.List;
import java.util.Optional;

public class EchoTalk extends Talk {
    
    public EchoTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext(Main.class);
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of(className(this), "echo.EchoTalk::echo", InputTrigger.ofRegex("/echo ?(.+)?")));
    }
    
    public TalkResponse echo(TalkRequest request) {
        List<String> regexMatches = request.getMatch().getRegexMatches();
        String echoThis = regexMatches.get(1);

        return echoThis != null ?
            directEcho(request, echoThis) :
            askAndEcho(request);
    }

    private TalkResponse directEcho(TalkRequest request, String echoThis) {
        return TalkResponse.ofText(
            className(this), request.getChatId(),
            echoThis
        );
    }

    public TalkResponse askAndEcho(TalkRequest call) {
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "Enter some text:"
        ).withRegex("echo.EchoTalk::echoIt", Triggers.NOT_A_COMMAND);
    }

    public TalkResponse echoIt(TalkRequest call) {
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "Your input: " + call.getText() +
                "\n" +
                "\nBack to /tests"
        ).withString("main.MainTalk::tests", "/tests");
    }
}

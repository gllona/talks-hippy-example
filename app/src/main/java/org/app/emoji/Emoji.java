package org.app.emoji;

import com.vdurmont.emoji.EmojiParser;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.Optional;

public class Emoji extends Talk {

    public Emoji(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("Main");
    }

    @Override
    public Optional<InputHandler> getDefaultHandler() {
        return Optional.of(
            InputHandler.of(className(this), "emoji.Emoji::parse",
                InputTrigger.ofString("/emoji"))
        );
    }

    public TalkResponse parse(TalkRequest request) {
        String lastEmojiText = getContext().getString(request.getChatId(), "LAST_EMOJI_TEXT");
        if (lastEmojiText != null) {
            request.getInputAdapter().sendResponse(
                TalkResponse.ofText(
                    className(this), request.getChatId(),
                    "Last Emoji text sent:"
                        + "\n"
                        + "\n" + lastEmojiText
                )
            );
        }

        return TalkResponse.ofText(
                className(this), request.getChatId(),
            "Send emoji 👋"
        ).withButton("emoji.Emoji::emojiButton", "Emoji 🕹 Button")
         .withRegex("emoji.Emoji::doParse", ".*");
    }

    public TalkResponse emojiButton(TalkRequest request) {
        return TalkResponse.ofText(
            className(this), request.getChatId(),
            "You pressed on:"
                + "\n"
                + "\n" + request.getText()
        ).withPreserveHandlers();
    }

    public TalkResponse doParse(TalkRequest request) {
        String textWithEmoji = request.getText();

        getContext().set(
            request.getChatId(),
            "LAST_EMOJI_TEXT",
            textWithEmoji
        );

        String parsedText = EmojiParser.parseToHtmlHexadecimal(textWithEmoji);

        // two different outputs according to ENCODE_EMOJI setting
        return TalkResponse.ofText(
            className(this), request.getChatId(),
            parsedText
        ).withPreserveHandlers();
    }
}

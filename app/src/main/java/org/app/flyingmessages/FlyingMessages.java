package org.app.flyingmessages;

import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class FlyingMessages extends Talk {

    public FlyingMessages(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of(className(this), "flyingmessages.FlyingMessages::main",
            InputTrigger.ofString("/flyingmessages")
        ));
    }

    public TalkResponse main(TalkRequest call) {
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "The text you enter now will disappear after some minutes. Also the first bot response will disappear."
                + "\n"
                + "\nFlying messages count = " + talksConfig.getInteractionsManager().getFlyingMessagesCount()
                + "\n"
                + "\nFirst enter number of minutes (1-99):"
        ).withRegex("flyingmessages.FlyingMessages::showFlyingMessages", "[1-9]?[0-9]");
    }

    public TalkResponse showFlyingMessages(TalkRequest call) {
        int minutes = Integer.parseInt(call.getText());

        call.fly(minutes, TimeUnit.MINUTES);
        //call.fly(); // this will use the default duration

        call.getInputAdapter().sendResponse(
            TalkResponse.ofText(
                className(this), call.getChatId(),
                "This text and your input above will disappear after " + minutes + " minutes."
            ).withFlying(minutes, TimeUnit.MINUTES)
             //.withFlying() // this will use the default duration
        );

        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "Flying messages count = " + talksConfig.getInteractionsManager().getFlyingMessagesCount()
                + "\n"
                + "\nTry again /flyingmessages or go to /start"
        );
    }
}

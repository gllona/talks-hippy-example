package org.app.groups;

import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;
import org.logicas.librerias.talks.engine.InputTrigger.GroupAction;

import java.util.Optional;

public class JoinGroup extends Talk {

    public JoinGroup(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of(className(this), "groups.JoinGroup::join", InputTrigger.ofGroupAction(GroupAction.JOIN)));
    }

    public TalkResponse join(TalkRequest call) {
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "Bot joined group!"
        );
    }
}

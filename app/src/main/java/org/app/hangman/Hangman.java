package org.app.hangman;

import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Locale;

/**
 * Based on <a href="https://gist.github.com/SedaKunda/79e1d9ddc798aec3a366919f0c14a078">Hangman by SedaKunda</a>.
 *
 * Words from <a href="https://github.com/javierarce/palabras">Lemario by Javier Arce</a>.
 */
public class Hangman {

    @RequiredArgsConstructor
    @Data
    public static class Status {
        private final String word;
        private final String asterisk;
        private final int count;
        private final String hangmanImage;
        private final String message;
        private final boolean finished;
    }

    private static final String[] words = readWordList();
    private static final String WORDS_FILE = "hangman-wordlist.txt";
    private static final String GUESS_ANY_LETTER_IN_THE_WORD = "Guess any letter in the word";
    private static final Locale LOCALE = new Locale("es");
    private static Hangman instance = null;

    public static synchronized Hangman getInstance() {
        if (instance == null) {
            instance = new Hangman();
        }
        return instance;
    }

    private static String[] readWordList() {
        return getResourceFileContent(WORDS_FILE);
    }

    private static String[] getResourceFileContent(String fileName) {
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        try (InputStream is = classLoader.getResourceAsStream(fileName)) {
            if (is == null) {
                throw new RuntimeException("Hangman: words file not found");
            }
            try (InputStreamReader isr = new InputStreamReader(is);
                BufferedReader reader = new BufferedReader(isr)) {
                return reader.lines().toArray(String[]::new);
            }
        } catch (IOException e) {
            throw new RuntimeException("Hangman: exception loading words file", e);
        }
    }

    public Status start() {
        String word = selectWord();
        Status status = new Status(
            word,
            new String(new char[word.length()]).replace("\0", "*"),
            0,
            null,
            GUESS_ANY_LETTER_IN_THE_WORD,
            false
        );
        return status;
    }

    private String selectWord() {
        String word = null;
        while (word == null || word.length() < 5) {
            word = words[(int) (Math.random() * words.length)];
        }
        return clean(word);
    }

    private String clean(String text) {
        return text
            .toLowerCase(LOCALE)
            .replace('á', 'a')
            .replace('é', 'e')
            .replace('í', 'i')
            .replace('ó', 'o')
            .replace('ú', 'u')
            .replace('ü', 'u')
            .toUpperCase(LOCALE);
    }

    public Status guess(Status status, String guess) {
        if (status.finished) {
            return status;
        }
        if (status.count < 7 && status.asterisk.contains("*")) {
            guess = clean(guess);
            return hang(status, guess);
        } else if (status.count < 7) {
            return new Status(
                status.word,
                status.asterisk,
                status.count + 1,
                status.hangmanImage,
                "You already won!!!",
                true
            );
        } else {
            return new Status(
                status.word,
                status.asterisk,
                status.count + 1,
                status.hangmanImage,
                "How a hanged man can talk?",
                true
            );
        }
    }

    private Status hang(Status status, String guess) {
        String word = status.word;
        String asterisk = status.asterisk;
        int count = status.count;
        String hangmanImage = null;
        boolean finished = false;
        String message = GUESS_ANY_LETTER_IN_THE_WORD;

        String newasterisk = "";
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) == guess.charAt(0)) {
                newasterisk += guess.charAt(0);
            } else if (asterisk.charAt(i) != '*') {
                newasterisk += word.charAt(i);
            } else {
                newasterisk += "*";
            }
        }

        if (asterisk.equals(newasterisk)) {
            count++;
            hangmanImage = hangmanImage(count, word);
            if (count >= 7) {
                message = "Good luck next time!";
                finished = true;
            }
        } else {
            asterisk = newasterisk;
        }

        if (asterisk.equals(word)) {
            message = "Correct! You win! The word was " + word;
            finished = true;
        }
        return new Status(word, asterisk, count, hangmanImage, message, finished);
    }

    private String hangmanImage(int count, String word) {
        if (count == 1) {
            return
                "Wrong guess, try again\n" +
                "\n" +
                "\n" +
                "\n" +
                "\n" +
                "___|___\n";
        }
        if (count == 2) {
            return
                "Wrong guess, try again\n" +
                "   |\n" +
                "   |\n" +
                "   |\n" +
                "   |\n" +
                "   |\n" +
                "   |\n" +
                "   |\n" +
                "___|___\n";
        }
        if (count == 3) {
            return
                "Wrong guess, try again\n" +
                "   ____________\n" +
                "   |\n" +
                "   |\n" +
                "   |\n" +
                "   |\n" +
                "   |\n" +
                "   |\n" +
                "   |\n" +
                "___|___\n";
        }
        if (count == 4) {
            return
                "Wrong guess, try again\n" +
                "   ____________\n" +
                "   |          _|_\n" +
                "   |         /   \\\n" +
                "   |        |     |\n" +
                "   |         \\_ _/\n" +
                "   |\n" +
                "   |\n" +
                "   |\n" +
                "___|___\n";
        }
        if (count == 5) {
            return
                "Wrong guess, try again\n" +
                "   ____________\n" +
                "   |          _|_\n" +
                "   |         /   \\\n" +
                "   |        |     |\n" +
                "   |         \\_ _/\n" +
                "   |           |\n" +
                "   |           |\n" +
                "   |\n" +
                "___|___\n";
        }
        if (count == 6) {
            return
                "Wrong guess, try again\n" +
                "   ____________\n" +
                "   |          _|_\n" +
                "   |         /   \\\n" +
                "   |        |     |\n" +
                "   |         \\_ _/\n" +
                "   |           |\n" +
                "   |           |\n" +
                "   |          / \\ \n" +
                "___|___      /   \\\n";
        }
        if (count == 7) {
            return "GAME OVER!\n" +
                "   ____________\n" +
                "   |          _|_\n" +
                "   |         /   \\\n" +
                "   |        |     |\n" +
                "   |         \\_ _/\n" +
                "   |          _|_\n" +
                "   |         / | \\\n" +
                "   |          / \\ \n" +
                "___|___      /   \\\n" +
                "GAME OVER!\n" +
                "The word was " + word + "\n";
        }

        return null;
    }
}

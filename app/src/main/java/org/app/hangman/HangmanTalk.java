package org.app.hangman;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.ToNumberPolicy;
import org.app.main.Main;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.Optional;

public class HangmanTalk extends Talk {

    private static final String HANGMAN_STATUS = "HANGMAN_STATUS";
    private static final String GUESS_REGEX = "[a-zA-ZáéíóúüÁÉÍÓÚÜñÑ]";

    private static Gson gson = new GsonBuilder()
        .disableHtmlEscaping()
        //.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
        //.setPrettyPrinting()
        .setObjectToNumberStrategy(ToNumberPolicy.LONG_OR_DOUBLE)
        .serializeNulls()
        .create();

    private final Hangman hangman = Hangman.getInstance();

    public HangmanTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext(Main.class);
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(
            InputHandler.of(className(this), "hangman.HangmanTalk::start", InputTrigger.ofString("/hangman"))
        );
    }

    public TalkResponse start(TalkRequest request) {
        Hangman.Status status = hangman.start();

        String statusJson = gson.toJson(status);
        context.set(request.getChatId(), HANGMAN_STATUS, statusJson);

        return TalkResponse.ofHtml(
            className(this), request.getChatId(),
            "<code>" + status.getAsterisk() + "</code>"
        ).next(
            TalkResponse.ofText(
                className(this), request.getChatId(),
                status.getMessage()
            ).withRegex("hangman.HangmanTalk::guess", GUESS_REGEX)
        );
    }

    public TalkResponse guess(TalkRequest request) {
        String guess = request.getText();
        String statusJson = context.getString(request.getChatId(), HANGMAN_STATUS);
        Hangman.Status status = gson.fromJson(statusJson, Hangman.Status.class);

        status = hangman.guess(status, guess);

        statusJson = gson.toJson(status);
        context.set(request.getChatId(), HANGMAN_STATUS, statusJson);

        TalkResponse finalResponse = TalkResponse.ofHtml(
            className(this), request.getChatId(),
            "<code>" + status.getAsterisk() + "</code>"
        ).next(
            TalkResponse.ofText(
                    className(this), request.getChatId(),
                status.getMessage()
            ).withRegexIf("hangman.HangmanTalk::guess", GUESS_REGEX, ! status.isFinished())
             .withButtonIf("main.MainTalk::hello", "Continue", status.isFinished())
        );

        if (status.getHangmanImage() == null) {
            return finalResponse;
        } else {
            return TalkResponse.ofHtml(
                className(this), request.getChatId(),
                "<pre>" + status.getHangmanImage() + "</pre>"
            ).next(finalResponse);
        }
    }
}

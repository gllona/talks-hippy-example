package org.app.hints;

import org.app.main.Main;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.Optional;

public class Hints extends Talk {

    public Hints(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext(Main.class);
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of(className(this), "hints.Hints::hints",
            InputTrigger.ofString("/hints")));
    }

    public TalkResponse hints(TalkRequest call) {
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Select an option, then check that message actions (hints) are shown in the Matrix client." +
                "\n" +
                "\nActions are not shown in Telegram." +
                "\n" +
                "\n* Go back to /start"
        ).withButton("hints.Hints::defaultHints", "Default")
         .withButton("hints.Hints::enableHints", "Enable hints")
         .withButton("hints.Hints::disableHints", "Disable hints");
    }

    public TalkResponse defaultHints(TalkRequest call) {
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "This message is defined with one button and should use the default hints configuration" +
                "\n" +
                "\n* Go back to /hints"
        ).withButton("hints.Hints::hints", "Continue");
    }

    public TalkResponse enableHints(TalkRequest call) {
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "This message is defined with one button and should show hints in every case" +
                "\n" +
                "\n* Go back to /hints"
        ).withButton("hints.Hints::hints", "Continue")
         .withHints();
    }

    public TalkResponse disableHints(TalkRequest call) {
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "This message is defined with one button and should not show any hint in every case" +
                "\n" +
                "\n* Go back to /hints"
        ).withButton("hints.Hints::hints", "Continue")
         .withNoHints();
    }
}

package org.app.html;

import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.Optional;

public class Html extends Talk {

    public Html(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("Main");
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of(className(this), "html.Html::main",
            InputTrigger.ofString("/html")));
    }

    public TalkResponse main(TalkRequest call) {
        return TalkResponse.ofHtml(
                className(this), call.getChatId(),
            call.getChannel(),
            "HTML test" +
                "\n" +
                "\n<b>Bold</b> text" +
                "\n<i>Italic</i> text" +
                "\n<u>Underline</u> text" +
                "\n<s>Strikethrough</s> text" +
                "\n<a href=\"https://core.telegram.org/bots/api#formatting-options\">Link</a> to Telegram formatting features" +
                "\nPlain URL: https://core.telegram.org/bots/" +
                "\n<span class=\"tg-spoiler\">Spoiler</span> text" +
                "\n<code>Code</code> text" +
                "\n<pre>Preformatted text line 1" +
                "\nPreformatted text line 2" +
                "\nPreformatted text line 3</pre>" +
                "\n" +
                "\nBack to /tests"
        ).withString("main.MainTalk::tests", "/tests")
         .withButton("html.Html::nextStep", "Continue");
    }

    public TalkResponse nextStep(TalkRequest call) {
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            call.getChannel(),
            "Just another step."
        ).withPreserveHandlers();
    }
}

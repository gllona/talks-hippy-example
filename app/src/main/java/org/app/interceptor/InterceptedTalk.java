package org.app.interceptor;

import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.Optional;

public class InterceptedTalk extends Talk {

    public InterceptedTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of(
            className(this), "interceptor.InterceptedTalk::main",
            InputTrigger.ofString("/interceptors")
        ));
    }

    public TalkResponse main(TalkRequest request) {
        return TalkResponse.ofText(
                className(this), request.getChatId(),
            "Follow chain?\n" +
                "\n" +
                "* Back to /start"
        ).withButton("interceptor.InterceptedTalk::saveInterceptionFlag", "Yes")
         .withButton("interceptor.InterceptedTalk::saveInterceptionFlag", "No");
    }

    public TalkResponse saveInterceptionFlag(TalkRequest request) {
        context.set(request.getChatId(), "FOLLOW_CHAIN", request.getText());

        return TalkResponse.ofText(
            className(this), request.getChatId(),
            "Saved follow chain preference. Next interception should apply it.\n" +
                "\n" +
                "* Back to /start"
        ).withButton("interceptor.InterceptedTalk::finish", "Continue");
    }

    public TalkResponse finish(TalkRequest request) {
        return TalkResponse.ofText(
            className(this), request.getChatId(),
            "Last message from intercepted Talk after applying interception status.\n" +
                "\n" +
                "* Back to /start"
        );
    }
}

package org.app.interceptor;

import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.List;
import java.util.Optional;

public class InterceptorTalk extends Interceptor {

    public InterceptorTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext(InterceptedTalk.class);
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.empty();
    }

    @Override
    public List<Talk> interceptedTalks() {
        return List.of(talkForClass(InterceptedTalk.class));
    }

    @Override
    public InterceptorResponse intercept(TalkRequest request) {
        String followChainText = context.getString(request.getChatId(), "FOLLOW_CHAIN");
        boolean followChain = ! "No".equals(followChainText);

        return InterceptorResponse.of(
            this,
            buildTalkResponse(request),
            followChain
        );
    }

    private TalkResponse buildTalkResponse(TalkRequest request) {
        return TalkResponse.ofText(
            className(this), request.getChatId(),
            "Message from interceptor Talk"
        ).withPreserveHandlers();
    }
}

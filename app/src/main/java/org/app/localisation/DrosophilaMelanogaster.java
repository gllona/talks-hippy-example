package org.app.localisation;

import org.app.Utils;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;
import org.logicas.librerias.translations.LangStrings;

import java.util.Collection;
import java.util.Optional;

public class DrosophilaMelanogaster extends Talk {

    private static String YAML_FILE = "drosophila_melanogaster.yml";

    private LangStrings strings = new LangStrings(YAML_FILE, this::getBotLang);

    public DrosophilaMelanogaster(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of(className(this), "localisation.DrosophilaMelanogaster::main",
            InputTrigger.ofString("/fly"),
            InputTrigger.ofString("/mosca")));
    }

    public String getBotLang(Object session) {
        String contextLang = getContext().getString(session.toString(), "LANG");
        return contextLang == null ? "en" : contextLang;
    }

    public TalkResponse main(TalkRequest call) {
        return selectLanguage(call);
    }

    public TalkResponse selectLanguage(TalkRequest call) {
        Collection<String> langs = strings.getLangs().values();

        TalkResponse talkResponse = TalkResponse.ofText(
            className(this), call.getChatId(),
            "Language:"
        );
        for (String lang : langs) {
            talkResponse.withButton("localisation.DrosophilaMelanogaster::doSelectLanguage", lang);
        }
        return talkResponse;
    }

    public TalkResponse doSelectLanguage(TalkRequest call) {
        String langName = call.getText();
        String lang = strings.getLangs().entrySet().stream()
            .filter(entry -> entry.getValue().equals(langName))
            .findFirst()
            .map(entry -> entry.getKey())
            .orElse(null);
        getContext().set(call.getChatId(), "LANG", lang);

        return whoTheFlyIs(call);
    }

    public TalkResponse whoTheFlyIs(TalkRequest call) {
        call.getInputAdapter().sendResponse(
            TalkResponse.ofPhoto(
                className(this), call.getChatId(),
                Utils.fileFrom(talksConfig, "drosophila.jpg")
            ).withMediaResourceId("drosophila-melanogaster")
        );

        return TalkResponse.ofText(
                className(this), call.getChatId(),
            strings.with("fly-name", strings.str("drosophila-melanogaster", call.getChatId()))
                   .str("who-the-fly-is", call.getChatId())
        )
        .withButton("localisation.DrosophilaMelanogaster::flyNameMeaning",
            strings.str("continue", call.getChatId()));
    }

    public TalkResponse flyNameMeaning(TalkRequest call) {
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            strings.str("fly-name-meaning", call.getChatId())
        )
        .withButton("localisation.DrosophilaMelanogaster::flyNameBecauseOf",
            strings.str("continue", call.getChatId()));
    }

    public TalkResponse flyNameBecauseOf(TalkRequest call) {
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            strings.str("fly-name-because-of", call.getChatId())
        )
        .withButton("localisation.DrosophilaMelanogaster::theEnd",
            strings.str("continue", call.getChatId()));
    }

    public TalkResponse theEnd(TalkRequest call) {
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            strings.str("the-end", call.getChatId())
        );
    }
}

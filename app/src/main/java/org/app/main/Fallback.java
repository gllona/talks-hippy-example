package org.app.main;

import org.app.AppSetting;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.config.LibSetting;
import org.logicas.librerias.talks.engine.*;
import spark.utils.StringUtils;

import java.util.Optional;

public class Fallback extends Talk {

    public Fallback(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext(Main.class);
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of(className(this), "main.Fallback::fallback", InputTrigger.ofRegex(".+")));
    }

    public TalkResponse fallback(TalkRequest request) {
        return defaultResponse(request);
    }

    public TalkResponse defaultResponse(TalkRequest request) {
        String message = AppSetting.unencode(LibSetting.get(LibSetting.FALLBACK_RESPONSE));

        return StringUtils.isBlank(message) ?
            TalkResponse.EMPTY.withPreserveHandlers() :
            TalkResponse.ofText(
                className(this), request.getChatId(),
                message
            ).withPreserveHandlers();
    }
}

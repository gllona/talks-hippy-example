package org.app.main;

import org.app.Strings;
import org.app.Strings.Lang;
import org.app.Strings.Str;
import org.app.Triggers;
import org.app.admin.UserService;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.List;
import java.util.Optional;

public class MainTalk extends Talk {

    private UserService userService = UserService.getInstance(talksConfig);
    private Strings strings = Strings.getInstance(this::getBotLang);

    public MainTalk(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext(Main.class);
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of(className(this), "main.MainTalk::main", InputTrigger.ofRegex("/start( .+)?")));
    }

    public Lang getBotLang(String session) {
        String contextLang = getContext().getString(session, "LANG");
        return contextLang == null ?
            Lang.ES :
            Lang.valueOf(contextLang);
    }

    public TalkResponse main(TalkRequest call) {
        Optional<TalkResponse> initialization = userService.firstTimeRoute(call.getChatId(), className(this));
        if (initialization.isPresent()) {
            return initialization.get();
        }

        userService.fillInUsers(call);

        List<String> matches = call.getMatch().getRegexMatches();
        String arguments = matches.size() == 1 ? null : matches.get(1);

        if (arguments != null) {
            return startWithArguments(call, arguments);
        }

        resetContext(call);

        return hello(call);
    }

    public TalkResponse hello(TalkRequest call) {
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Hola mundo!" +
                "\n" +
                "\nUsername: " + call.getUserShortName() +
                "\nUser ID: " + call.getUsername() +
                "\nChat ID: " + call.getChatId() +
                "\n" +
                "\nArea de pruebas - test area: /tests" +
                "\nBot source code: https://gitlab.com/gllona/talks-hippy-example" +
                "\nBot framework: https://gitlab.com/gllona/talks-hippy" +
                "\n" +
                "\nJugar al ahorcado - play hangman (all words in Spanish): /hangman"
        )
        .withString("main.MainTalk::tests", "/tests");
    }

    public TalkResponse tests(TalkRequest call) {
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "This is the Menu" +
                "\n" +
                "\nApp de Xarxa: /menu (demo - no usar para contactos reales - not for real contacts)" +
                "\n" +
                "\n/helloworld" +
                "\n/purpose" +
                "\n/echo" +
                "\n/settings (regex)" +
                "\n/eatings (database)" +
                "\n/media" +
                "\n/buttons" +
                "\n/html" +
                "\n/handlers" +
                "\n/priorities" +
                "\n/concurrency" +
                "\n/interceptors" +
                "\n/hints" +
                "\n/flyingmessages" +
                "\n/messageIds" +
                "\n/emoji" +
                "\n/fly (localisation - media resource IDs)" +
                "\n/contact (anonymous users-operators communication)" +
                "\n/admin (restricted operations)" +
                "\n/start (reset bot)"
        )
        .withString("main.MainTalk::helloworld", "/helloworld")
        .withString("main.MainTalk::purpose", "/purpose")
        .withString("main.MainTalk::settings", "/settings");
    }

    public TalkResponse helloworld(TalkRequest call) {
        // this method for localising strings is deprecated. For an updated method see package org.app.localisation
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            strings.get(Str.HelloWorld).forSession(call.getChatId()) +
                "\n" +
                strings.get(Str.YourNameIs).with("userName", call.getUserShortName()).forSession(call.getChatId()) +
                "\n/helloworld"
        )
        .withString("main.MainTalk::helloworld", "/helloworld");
    }

    public TalkResponse purpose(TalkRequest call) {
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "The purpose is to act now"
        );
    }

    public TalkResponse echo(TalkRequest call) {
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "Enter some text:"
        ).withRegex("main.MainTalk::echoIt", Triggers.NOT_A_COMMAND);
    }

    public TalkResponse echoIt(TalkRequest call) {
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "Your input: " + call.getText() +
                "\n" +
                "\nBack to /tests"
        ).withString("main.MainTalk::tests", "/tests");
    }

    public TalkResponse settings(TalkRequest call) {
        call.getInputAdapter().sendResponse(TalkResponse.ofText(className(this), call.getChatId(), "Message 1"));
        call.getInputAdapter().sendResponse(TalkResponse.ofText(className(this), call.getChatId(), "Message 2"));
        call.getInputAdapter().sendResponse(TalkResponse.ofText(className(this), call.getChatId(), "Message 3"));
        call.getInputAdapter().sendResponse(TalkResponse.ofText(className(this), call.getChatId(), "Message 4"));

        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Settings are not implemented yet but you can test:" +
                "\n/setting_666"
        )
        .withRegex("main.MainTalk::settingN", "/setting_([0-9]+)");
    }

    public TalkResponse settingN(TalkRequest call) {
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "This is setting #" + call.getMatch().getRegexMatches().get(1)
        );
    }

    public TalkResponse method(TalkRequest call) {
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "HEADER" +
                "\n/COMMAND"
        )
        .withString("main.MainTalk::method", "/COMMAND");   // change main.MainTalk::method
    }

    private TalkResponse startWithArguments(TalkRequest call, String arguments) {
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "/start with arguments is not supported yet"
        );
    }

    private void resetContext(TalkRequest call) {
        talksConfig.getTalkManager().resetTalkContexts(call.getChatId());
    }
}

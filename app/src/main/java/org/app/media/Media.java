package org.app.media;

import org.logicas.librerias.talks.api.FileSystemApi;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.io.File;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

public class Media extends Talk {

    FileSystemApi fileSystem = talksConfig.getFileSystem();

    public Media(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of(className(this), "media.Media::menu", InputTrigger.ofString("/media")));
    }

    public TalkResponse menu(TalkRequest call) {
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "Select one:"
        ).withButton("media.Media::files", "Use files")
         .withButton("media.Media::fileIds", "Use file IDs");
    }

    public TalkResponse files(TalkRequest call) {
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "What to upload:" +
                "\n" +
                "\n/location" +
                "\n/photo" +
                "\n/wrongPhoto" +
                "\n/animation" +
                "\n/video" +
                "\n/videoNote" +
                "\n/audio" +
                "\n/voice" +
                "\n/document"
        )
        .withString("media.Media::inputLocation", "/location")
        .withString("media.Media::inputPhoto", "/photo")
        .withString("media.Media::sendWrongPhoto", "/wrongPhoto")
        .withString("media.Media::inputAnimation", "/animation")
        .withString("media.Media::inputVideo", "/video")
        .withString("media.Media::inputVideoNote", "/videoNote")
        .withString("media.Media::inputAudio", "/audio")
        .withString("media.Media::inputVoice", "/voice")
        .withString("media.Media::inputDocument", "/document");
    }

    public TalkResponse fileIds(TalkRequest call) {
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "What to upload:" +
                "\n" +
                "\n/location" +
                "\n/photo" +
                "\n/animation" +
                "\n/video" +
                "\n/videoNote" +
                "\n/audio" +
                "\n/voice" +
                "\n/document"
        )
        .withString("media.Media::inputLocation", "/location")
        .withString("media.Media::inputPhotoId", "/photo")
        .withString("media.Media::inputAnimationId", "/animation")
        .withString("media.Media::inputVideoId", "/video")
        .withString("media.Media::inputVideoNoteId", "/videoNote")
        .withString("media.Media::inputAudioId", "/audio")
        .withString("media.Media::inputVoiceId", "/voice")
        .withString("media.Media::inputDocumentId", "/document");
    }

    public TalkResponse inputLocation(TalkRequest call) {
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Please attach a location now:"
        )
       .withMedia("media.Media::uploadLocation", MediaType.LOCATION);
    }

    public TalkResponse uploadLocation(TalkRequest call) {
        TalkLocation location = (TalkLocation)call.getMatch().getMediaMatch();
        call.getInputAdapter().sendResponse(TalkResponse.ofText(
            className(this), call.getChatId(),
            "The location you sent is: LAT=" + location.getLatitude() + " LON=" + location.getLongitude() +
                "\nThis is the location back:"
            )
        );
        call.getInputAdapter().sendResponse(TalkResponse.ofLocation(
            className(this), call.getChatId(),
            location.getLatitude(),
            location.getLongitude()
        ));
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "Go to /media again"
        );
    }

    public TalkResponse inputPhoto(TalkRequest call) {
        return inputMedia(call, MediaType.PHOTO, "photo");
    }

    public TalkResponse inputAnimation(TalkRequest call) {
        return inputMedia(call, MediaType.ANIMATION, "animation");
    }

    public TalkResponse inputVideo(TalkRequest call) {
        return inputMedia(call, MediaType.VIDEO, "video");
    }

    public TalkResponse inputVideoNote(TalkRequest call) {
        return inputMedia(call, MediaType.VIDEO_NOTE, "videoNote");
    }

    public TalkResponse inputAudio(TalkRequest call) {
        return inputMedia(call, MediaType.AUDIO, "audio");
    }

    public TalkResponse inputVoice(TalkRequest call) {
        return inputMedia(call, MediaType.VOICE, "voice");
    }

    public TalkResponse inputDocument(TalkRequest call) {
        return inputMedia(call, MediaType.DOCUMENT, "document");
    }

    public TalkResponse inputPhotoId(TalkRequest call) {
        return inputMediaId(call, MediaType.PHOTO, "photo");
    }

    public TalkResponse inputAnimationId(TalkRequest call) {
        return inputMediaId(call, MediaType.ANIMATION, "animation");
    }

    public TalkResponse inputVideoId(TalkRequest call) {
        return inputMediaId(call, MediaType.VIDEO, "video");
    }

    public TalkResponse inputVideoNoteId(TalkRequest call) {
        return inputMediaId(call, MediaType.VIDEO_NOTE, "videoNote");
    }

    public TalkResponse inputAudioId(TalkRequest call) {
        return inputMediaId(call, MediaType.AUDIO, "audio");
    }

    public TalkResponse inputVoiceId(TalkRequest call) {
        return inputMediaId(call, MediaType.VOICE, "voice");
    }

    public TalkResponse inputDocumentId(TalkRequest call) {
        return inputMediaId(call, MediaType.DOCUMENT, "document");
    }

    private TalkResponse inputMedia(TalkRequest call, MediaType mediaType, String mediaTypeName) {
        context.set(call.getChatId(), "MEDIA_TYPE_NAME", mediaTypeName);
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            String.format("Please attach the %s now:", mediaTypeName)
        ).withMedia("media.Media::uploadMedia", mediaType);
    }

    private TalkResponse inputMediaId(TalkRequest call, MediaType mediaType, String mediaTypeName) {
        context.set(call.getChatId(), "MEDIA_TYPE_NAME", mediaTypeName);
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            String.format("Please attach the %s now:", mediaTypeName)
        ).withMedia("media.Media::uploadMediaId", mediaType);
    }

    public TalkResponse uploadMedia(TalkRequest call) {
        File file = call.getFile();
        String fileId = call.getFileId();

        if (file == null) {
            return TalkResponse.ofText(
                className(this), call.getChatId(),
                "Error getting the file contents."
            );
        }

        fileSystem.deleteDownloadedAfter(file, 5, TimeUnit.MINUTES);

        String mediaTypeName = context.getString(call.getChatId(), "MEDIA_TYPE_NAME");

        call.getInputAdapter().sendResponse(TalkResponse.ofText(
            className(this), call.getChatId(),
            String.format("The %s you sent is: FILE_ID=[%s], FILE_NAME=[%s]", mediaTypeName, fileId, file.getName()) +
                "\n" +
                String.format("\nAnd this is your %s back:", mediaTypeName)
            )
        );

        call.getInputAdapter().sendResponse(buildMediaResponse(call, file, mediaTypeName));

        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "Go to /media again"
        );
    }

    public TalkResponse uploadMediaId(TalkRequest call) {
        String fileId = call.getFileId();

        if (fileId == null) {
            return TalkResponse.ofText(
                className(this), call.getChatId(),
                "Error getting the file ID."
            );
        }

        String mediaTypeName = context.getString(call.getChatId(), "MEDIA_TYPE_NAME");

        call.getInputAdapter().sendResponse(TalkResponse.ofText(
            className(this), call.getChatId(),
            String.format("The %s you sent is: FILE_ID=[%s]", mediaTypeName, fileId) +
                "\n" +
                String.format("\nAnd this is your %s back:", mediaTypeName)
            )
        );

        call.getInputAdapter().sendResponse(buildMediaIdResponse(call, fileId, mediaTypeName));

        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "Go to /media again"
        );
    }

    private TalkResponse buildMediaResponse(TalkRequest call, File file, String mediaTypeName) {
        final String chatId = call.getChatId();
        return switch(mediaTypeName) {
            case "photo" -> TalkResponse.ofPhoto(className(this), chatId, file);
            case "animation" -> TalkResponse.ofAnimation(className(this), chatId, file);
            case "video" -> TalkResponse.ofVideo(className(this), chatId, file);
            case "videoNote" -> TalkResponse.ofVideoNote(className(this), chatId, file);
            case "audio" -> TalkResponse.ofAudio(className(this), chatId, file);
            case "voice" -> TalkResponse.ofVoice(className(this), chatId, file);
            case "document" -> TalkResponse.ofDocument(className(this), chatId, file);
            default -> null;
        };
    }

    private TalkResponse buildMediaIdResponse(TalkRequest call, String fileId, String mediaTypeName) {
        final String chatId = call.getChatId();
        return switch(mediaTypeName) {
            case "photo" -> TalkResponse.ofPhotoId(className(this), chatId, fileId);
            case "animation" -> TalkResponse.ofAnimationId(className(this), chatId, fileId);
            case "video" -> TalkResponse.ofVideoId(className(this), chatId, fileId);
            case "videoNote" -> TalkResponse.ofVideoNoteId(className(this), chatId, fileId);
            case "audio" -> TalkResponse.ofAudioId(className(this), chatId, fileId);
            case "voice" -> TalkResponse.ofVoiceId(className(this), chatId, fileId);
            case "document" -> TalkResponse.ofDocumentId(className(this), chatId, fileId);
            default -> null;
        };
    }

    public TalkResponse sendWrongPhoto(TalkRequest call) {
        File wrongFile = new File("/");

        call.getInputAdapter().sendResponse(TalkResponse.ofText(
            className(this), call.getChatId(),
            "A wrong photo will be sent:"
            )
        );
        call.getInputAdapter().sendResponse(TalkResponse.ofPhoto(
            className(this), call.getChatId(),
            wrongFile
        ));
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "Go to /media again"
        );
    }
}

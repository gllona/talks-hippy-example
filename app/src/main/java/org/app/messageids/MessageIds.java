package org.app.messageids;

import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.Optional;

public class MessageIds extends Talk {

    public MessageIds(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of(className(this), "messageids.MessageIds::main",
            InputTrigger.ofString("/messageIds")
        ));
    }

    public TalkResponse main(TalkRequest call) {
        TalkResponse response = TalkResponse.ofText(
            className(this), call.getChatId(),
            "The message ID of the message you just sent is: " + call.getMessageId()
        ).withMessageIdGetter();

        talksConfig.getMessageSender().send(response);

        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "The message ID of the message you just received is: " + response.getMessageId() +
                "\n" +
                "\nBack to /start"
        );
    }
}

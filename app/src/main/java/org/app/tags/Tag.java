package org.app.tags;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.logicas.librerias.copersistance.entities.SingleKeyEntity;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@Setter
@Getter
@ToString
public class Tag extends SingleKeyEntity {

    public static final String FIELD_ID = "id";
    public static final String FIELD_CHAT_ID = "chat_id";
    public static final String FIELD_TAG = "tag";
    public static final String FIELD_VALUE = "value";

    public static final String[] FIELDS = {
        FIELD_ID,
        FIELD_CHAT_ID,
        FIELD_TAG,
        FIELD_VALUE
    };

    private static final String[] KEYS = {
        FIELD_ID
    };

    private Long id;
    private String chatId;
    private String tag;
    private String value;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Map<String, Object> parameters() {
        Map<String, Object> params = new HashMap<>();

        conditionalAddToParams(id, FIELD_ID, params);
        conditionalAddToParams(chatId, FIELD_CHAT_ID, params);
        conditionalAddToParams(tag, FIELD_TAG, params);
        conditionalAddToParams(value, FIELD_VALUE, params);

        return params;
    }

    @Override
    public String[] getKeyColumns() {
        return KEYS;
    }
}

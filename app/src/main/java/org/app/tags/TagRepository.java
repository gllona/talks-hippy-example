package org.app.tags;

import org.logicas.librerias.copersistance.Dao;
import org.logicas.librerias.copersistance.repositories.SingleKeyEntityRepository;

import java.util.List;
import java.util.Objects;

public class TagRepository extends SingleKeyEntityRepository<Tag> {

    private static TagRepository instance;

    private Dao dao;

    private TagRepository(Dao dao) {
        this.dao = dao;
    }

    public static synchronized TagRepository getInstance(Dao dao) {
        if (instance == null) {
            instance = new TagRepository(dao);
        }
        return instance;
    }

    private static final String TABLE = "chat_id_tags";

    @Override
    protected Dao dao() {
        return dao;
    }

    @Override
    public String tableName() {
        return TABLE;
    }

    @Override
    protected String[] columnNames() {
        return Tag.FIELDS;
    }

    @Override
    public String idFieldName() {
        return Tag.FIELD_ID;
    }

    public void set(String chatId, String tag, String value) {
        List<Tag> tags = readByField(Tag.FIELD_CHAT_ID, chatId, false, false);

        Tag existingTag = tags.stream()
            .filter(entry -> Objects.equals(tag, entry.getTag()))
            .findFirst()
            .orElse(null);

        if (existingTag == null) {
            Tag newTag = new Tag(null, chatId, tag, value);
            save(newTag);
        } else {
            existingTag.setValue(value);
            save(existingTag);
        }
    }

    public String get(String chatId, String tag) {
        List<Tag> tags = readByField(Tag.FIELD_CHAT_ID, chatId, false, false);

        String value = tags.stream()
            .filter(entry -> Objects.equals(tag, entry.getTag()))
            .findFirst()
            .map(Tag::getValue)
            .orElse(null);

        return value;
    }
}

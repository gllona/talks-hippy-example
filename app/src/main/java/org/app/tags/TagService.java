package org.app.tags;

import org.app.communicator.CommunicatorService;
import org.logicas.librerias.talks.api.TalksConfiguration;

public class TagService {

    private static TagService instance;

    private final TalksConfiguration talksConfig;
    private final CommunicatorService communicatorService;
    private final TagRepository repository;

    public TagService(TalksConfiguration talksConfig) {
        this.talksConfig = talksConfig;
        repository = TagRepository.getInstance(talksConfig.getDao());
        communicatorService = CommunicatorService.getInstance(talksConfig);
    }

    public static synchronized TagService getInstance(TalksConfiguration talksConfig) {
        if (instance == null) {
            return new TagService(talksConfig);
        }
        return instance;
    }

    public String get(String chatId, String tag) {
        return repository.get(chatId, tag);
    }

    public void set(String chatId, String tag, String value) {
        repository.set(chatId, tag, value);
        notifyRoomTagged(chatId, tag, value);
    }

    private void notifyRoomTagged(String chatId, String tag, String value) {
        String message = String.format("Tag '%s' set to '%s' for chatId %s", tag, value, chatId);
        communicatorService.sendToAdmins(message);
    }
}

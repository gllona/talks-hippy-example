package org.app.xarxa;

import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;

public class Xarxa extends Talk {

    // simulacion
    private static boolean esAdministrador = true;

    public Xarxa(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext();
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of(className(this), "xarxa.Xarxa::menu", InputTrigger.ofRegex("/menu|/(persona|ofrecimiento|peticion|soporte)_([A-Z0-9]+)")));
    }

    // MENU PRINCIPAL

    public TalkResponse menu(TalkRequest call) {
        List<String> matches = call.getMatch().getRegexMatches();
        if (matches.size() == 3 && matches.get(1) != null && matches.get(2) != null) {
            call.getMatch().setRegexMatches(asList(null, matches.get(2)));
            if (matches.get(1).equalsIgnoreCase("persona")) {
                return talkForClass(XarxaPersonas.class).persona(call);
            } else if (matches.get(1).equalsIgnoreCase("ofrecimiento")) {
                return talkForClass(XarxaOfrecimientos.class).ofrecimiento(call);
            } else if (matches.get(1).equalsIgnoreCase("peticion")) {
                return talkForClass(XarxaPeticiones.class).peticion(call);
            } else if (matches.get(1).equalsIgnoreCase("soporte")) {
                return talkForClass(XarxaSoportes.class).soporte(call);
            }
        }

        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Bienvenido a la app de la Xarxa de Soporte Mutuo" +
                "\n/ofrecer soporte" +
                "\n/solicitar soporte" +
                (esAdministrador ? "\nGestionar /soportes" : "") +
                (esAdministrador ? "\nPerfiles de /personas" : "\n/personas Tu perfil de persona") +
                "\n/contacto"
        )
       .withString("xarxa.Xarxa::contacto", "/contacto");
    }

    // CONTACTO

    public TalkResponse contacto(TalkRequest call) {
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Xarxa de Soporte Mutuo" +
                "\n\n✉️ correo: xarxa@provider.com" +
                "\n\n☎️ número de teléfono: +34888888888" +
                "\n\n/menu principal"
            )
            .withString("xarxa.Xarxa::menu", "/menu");
    }
}

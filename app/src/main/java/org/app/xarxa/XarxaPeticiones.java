package org.app.xarxa;

import org.app.xarxa.XarxaHelper.TipoBusqueda;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class XarxaPeticiones extends Talk {

    // simulacion
    boolean esAdministrador = true;

    private static XarxaHelper helper;

    public XarxaPeticiones(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("Xarxa");
        helper = XarxaHelper.getInstance(talksConfig.getDao());
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of(className(this), "xarxa.XarxaPeticiones::menu", InputTrigger.ofString("/solicitar")));
    }

    // MENU PRINCIPAL

    public TalkResponse menu(TalkRequest call) {
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "/crear nueva petición" +
                "\nVer /mis peticiones" +
                "\n/buscar peticiones"
        )
       .withString("xarxa.XarxaPeticiones::nuevaPeticion", "/crear")
       .withString("xarxa.XarxaPeticiones::misPeticiones", "/mis")
       .withString("xarxa.XarxaPeticiones::buscarPeticion", "/buscar");
    }

    // NUEVA PETICION

    public TalkResponse nuevaPeticion(TalkRequest call) {
        Map<String, String> categorias = helper.getCategorias();
        String textoCategorias = categorias.entrySet().stream()
            .map(entry -> "* /" + entry.getKey() + " " + entry.getValue())
            .collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Selecciona una categoría según lo que necesitas:\n" +
                textoCategorias +
                (esAdministrador ? "\n/menu principal" : "")
            )
            .withRegex("xarxa.XarxaPeticiones::agregarDescripcion", "/(.*)");
    }

    public TalkResponse agregarDescripcion(TalkRequest call) {
        String slugCategoria = call.getMatch().getRegexMatches().get(1);
        String descripcionCategoria = helper.getCategorias().get(slugCategoria);

        if (descripcionCategoria == null) {
            return TalkResponse.ofText(
                    className(this), call.getChatId(),
                "No se ha reconocido la categoria" +
                    "\nVolver a seleccionar /categoria"
                )
                .withString("xarxa.XarxaPeticiones::seleccionarCategoria", "/categoria");
        }

        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Has seleccionado tu necesidad:" +
                "\n* " + descripcionCategoria +
                "\nIntroduce detalles de tu necesidad:"
            )
            .withRegex("xarxa.XarxaPeticiones::guardarPeticion", "(.*)");
    }

    public TalkResponse guardarPeticion(TalkRequest call) {
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "Se ha guardado tu peticion, los coordinadores han sido notificados y " +
                    "las personas que ofrecen ayuda pueden ver tu necesidad y ayudarte" +
                "\nOfrecimiento: /ofrecimiento_" +
                    helper.getHashidPeticion() +
                "\n**Gracias!** recuerda estar pendiente de las notificaciones" +
                "\n/menu principal"
            );
    }

    // MIS PETICIONES

    public TalkResponse misPeticiones(TalkRequest call) {
        Map<String, String> peticiones = helper.getMisPeticiones();
        String textoPeticiones = peticiones.entrySet().stream().map(
            entry -> "* /peticion_" + entry.getKey() + " " + entry.getValue()
        ).collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Estas son tus peticiones:\n" +
                textoPeticiones +
                "\n(Selecciona una petición para tenerla en memoria)" +
                "\n/solicitar soporte" +
                "\n/menu principal"
        )
        .withRegex("xarxa.XarxaPeticiones::peticion", "/peticion_([A-Z0-9]+)");
    }

    // BUSCAR PETICIONES

    public TalkResponse buscarPeticion(TalkRequest call) {
        helper.resetearCriterio(TipoBusqueda.PETICION, call.getChatId(), this);
        return criterioBusqueda(call);
    }

    public TalkResponse criterioBusqueda(TalkRequest call) {
        String criterio = helper.construirTextoCriterio(TipoBusqueda.PETICION, call.getChatId(), this);

        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Fijar criterios:" +
                "\n* /nombre de persona" +
                "\n* Usuario de /telegram" +
                "\n* /categoria de soporte" +
                "\n* Rango de /fechas" +
                "\n* Si tiene soportes /asignados" +
                "\n\nEl criterio actual de busqueda es:" +
                "\n" + (criterio.isEmpty() ? "NO HAY CRITERIOS FIJADOS" : criterio) +
                "\n\n/limpiar criterios de búsqueda" +
                "\n/buscar según los criterios fijados"
        )
        .withString("xarxa.XarxaPeticiones::criterioNombre", "/nombre")
        .withString("xarxa.XarxaPeticiones::criterioTelegram", "/telegram")
        .withString("xarxa.XarxaPeticiones::criterioCategoria", "/categoria")
        .withString("xarxa.XarxaPeticiones::criterioFechas", "/fechas")
        .withString("xarxa.XarxaPeticiones::criterioConSoportes", "/asignados")
        .withString("xarxa.XarxaPeticiones::limpiarCriterio", "/limpiar")
        .withString("xarxa.XarxaPeticiones::efectuarBusqueda", "/buscar");
    }

    public TalkResponse limpiarCriterio(TalkRequest call) {
        call.getInputAdapter().sendResponse(TalkResponse.ofText(className(this), call.getChatId(),
            "Se han limpiado los criterios de búsqueda"
        ));
        return buscarPeticion(call);
    }

    public TalkResponse criterioNombre(TalkRequest call) {
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Introduce el nombre de la persona o una parte del nombre:" +
                "\nIr a /criterios"
        )
                           .withRegex("xarxa.XarxaPeticiones::criterioNombreFijar", ".*");
    }

    public TalkResponse criterioNombreFijar(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        getContext().set(call.getChatId(), "peticion_nombre", call.getMatch().getRegexMatches().get(0));
        call.getInputAdapter().sendResponse(TalkResponse.ofText(className(this), call.getChatId(),
            "Se ha asignado el criterio de búsqueda por nombre de persona"
        ));
        return criterioBusqueda(call);
    }

    public TalkResponse criterioTelegram(TalkRequest call) {
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Introduce el usuario de Telegram de la persona:" +
                "\nIr a /criterios"
        )
                           .withRegex("xarxa.XarxaPeticiones::criterioTelegramFijar", ".*");
    }

    public TalkResponse criterioTelegramFijar(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        call.getInputAdapter().sendResponse(TalkResponse.ofText(className(this), call.getChatId(),
            "Se ha asignado el criterio de búsqueda por usuario de Telegram"
        ));
        getContext().set(call.getChatId(), "peticion_telegram", call.getMatch().getRegexMatches().get(0));
        return criterioBusqueda(call);
    }

    public TalkResponse criterioCategoria(TalkRequest call) {
        Map<String, String> categorias = helper.getCategorias();
        String textoCategorias = categorias.entrySet().stream()
                                           .map(entry -> "* /" + entry.getKey() + " " + entry.getValue())
                                           .collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Selecciona una categoría por la que quieras filtrar:\n" +
                textoCategorias +
                "\n\nIr a /criterios"
        )
        .withRegex("xarxa.XarxaPeticiones::criterioCategoriaFijar", "/(.*)");
    }

    public TalkResponse criterioCategoriaFijar(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        getContext().set(call.getChatId(), "peticion_categoria", call.getMatch().getRegexMatches().get(1));
        call.getInputAdapter().sendResponse(TalkResponse.ofText(className(this), call.getChatId(),
            "Se ha asignado el criterio de búsqueda por categoría"
        ));
        return criterioBusqueda(call);
    }

    public TalkResponse criterioFechas(TalkRequest call) {
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Introduce la fecha inicial en formato DD-MM-AAAA (también puedes introducir /hoy o /ayer):" +
                "\nIr a /criterios"
        )
        .withRegex("xarxa.XarxaPeticiones::criterioFechas2", XarxaHelper.REGEX_FECHA_CRITERIOS);
    }

    public TalkResponse criterioFechas2(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        String fechaDesde = helper.construirFecha(call.getUpdate().message().text(), call.getMatch().getRegexMatches(), 1, false, true);
        getContext().set(call.getChatId(), "peticion_fechaDesde", fechaDesde);
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Introduce la fecha final en formato DD-MM-AAAA (también puedes introducir /hoy o /ayer o escribe /ok para usar la misma fecha inicial):" +
                "\nIr a /criterios"
        )
        .withRegex("xarxa.XarxaPeticiones::criterioFechasFijar", XarxaHelper.REGEX_FECHA_CRITERIOS_OK);
    }

    public TalkResponse criterioFechasFijar(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        String fechaHasta = call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/ok") ?
            getContext().getString(call.getChatId(), "peticion_fechaDesde") :
            helper.construirFecha(call.getUpdate().message().text(), call.getMatch().getRegexMatches(), 1, false, true);
        getContext().set(call.getChatId(), "peticion_fechaHasta", fechaHasta);

        call.getInputAdapter().sendResponse(TalkResponse.ofText(className(this), call.getChatId(),
            "Se ha asignado el criterio de búsqueda por rango de fechas"
        ));
        return criterioBusqueda(call);
    }

    public TalkResponse criterioConSoportes(TalkRequest call) {
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Indica si quieres filtrar según si los ofrecimientos tienen asignados soportes o no:" +
                "\n/si" +
                "\n/no" +
                "\n/indiferente" +
                "\n\nIr a /criterios"
        )
        .withRegex("xarxa.XarxaPeticiones::criterioConSoportesFijar", "/(.*)");
    }

    public TalkResponse criterioConSoportesFijar(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        String conSoportes = call.getMatch().getRegexMatches().get(1);
        if (conSoportes.equalsIgnoreCase("indiferente")) {
            conSoportes = null;
        }
        getContext().set(call.getChatId(), "peticion_conSoportes", conSoportes);

        call.getInputAdapter().sendResponse(TalkResponse.ofText(className(this), call.getChatId(),
            "Se ha asignado el criterio de búsqueda por si hay asignados o no soportes"
        ));
        return criterioBusqueda(call);
    }

    public TalkResponse efectuarBusqueda(TalkRequest call) {
        Map<String, String> peticiones = helper.getPeticionesBuscadas();
        String textoPeticiones = peticiones.entrySet().stream().map(
            entry -> "* /peticion_" + entry.getKey() + " " + entry.getValue()
        ).collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Estos son las peticiones encontradas según el criterio fijado:" +
                "\n\n" + textoPeticiones +
                "\n\n(Selecciona una petición para tenerla en memoria)" +
                "\nContinuar la /busqueda" +
                "\n/menu principal"
        )
        .withString("xarxa.XarxaPeticiones::criterioBusqueda", "/busqueda");
    }

    // FICHA PETICION

    public TalkResponse peticion(TalkRequest call) {
        String hashidPeticion = call.getMatch().getRegexMatches().get(1);
        String fichaPeticion = helper.getFichaPeticion(hashidPeticion);

        if (fichaPeticion != null) {
            getContext().set(call.getChatId(), "hashid_peticion", hashidPeticion);
            call.getInputAdapter().sendResponse(TalkResponse.ofText(className(this), call.getChatId(),
                fichaPeticion
            ));
        }
        return TalkResponse.ofText(
            className(this), call.getChatId(),
            (fichaPeticion == null ?
                    "NO SE HA ENCONTRADO UNA PETICION CON EL CODIGO INDICADO" :
                    "(Se ha guardado la petición en la memoria)") +
                "\n/solicitar soporte" +
                (esAdministrador ? "\nGestionar /soportes" : "") +
                "\n/menu principal"
        );
    }
}

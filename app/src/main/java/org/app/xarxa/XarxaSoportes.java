package org.app.xarxa;

import org.app.xarxa.XarxaHelper.TipoBusqueda;
import org.logicas.librerias.talks.api.TalkType;
import org.logicas.librerias.talks.api.TalksConfiguration;
import org.logicas.librerias.talks.engine.*;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class XarxaSoportes extends Talk {

    // simulacion
    boolean esAdministrador = true;

    private static XarxaHelper helper;

    public XarxaSoportes(TalksConfiguration talksConfig, TalkType talkType) {
        super(talksConfig, talkType);
        createContext("Xarxa");
        helper = XarxaHelper.getInstance(talksConfig.getDao());
    }

    @Override
    protected Optional<InputHandler> getDefaultHandler() {
        return Optional.of(InputHandler.of(className(this), "xarxa.XarxaSoportes::menu", InputTrigger.ofString("/soportes")));
    }

    // MENU PRINCIPAL

    public TalkResponse menu(TalkRequest call) {
        // chequear si el usuario puede ver soportes

        String hashidOfrecimiento = getContext().getString(call.getChatId(), "hashid_ofrecimiento");
        String hashidPeticion = getContext().getString(call.getChatId(), "hashid_peticion");

        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "/asignar nuevo soporte" +
                "\nfijar /peticion" +
                "\nfijar /ofrecimiento" +
                (hashidPeticion != null && hashidOfrecimiento != null ?
                    "\n/asignar /peticion_" + hashidPeticion + " a /ofrecimiento_" + hashidOfrecimiento : "") +
                "\n/buscar soporte"
            )
            .withString("xarxa.XarxaPeticiones::buscarPeticion", "/peticion")
            .withString("xarxa.XarxaOfrecimientos::buscarOfrecimiento", "/ofrecimiento")
            .withString("xarxa.XarxaSoportes::asignarSoporte", "/asignar")
            .withString("xarxa.XarxaSoportes::buscarSoporte", "/buscar");
    }

    // ASIGNAR SOPORTE

    public TalkResponse asignarSoporte(TalkRequest call) {
        String hashidOfrecimiento = getContext().getString(call.getChatId(), "hashid_ofrecimiento");
        String hashidPeticion = getContext().getString(call.getChatId(), "hashid_peticion");

        if (hashidPeticion == null) {
            return TalkResponse.ofText(
                    className(this), call.getChatId(),
                "Primero fija una peticion en memoria"
                )
                .withPreserveHandlers();
        }
        if (hashidOfrecimiento == null) {
            return TalkResponse.ofText(
                    className(this), call.getChatId(),
                "Primero fija un ofrecimiento en memoria"
                )
                .withPreserveHandlers();
        }

        // asignar peticion a ofrecimiento

        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Introduce una descripción para el soporte que se está asignando:" +
                "\nRegresar a asignar /soportes"
            )
            .withRegex("xarxa.XarxaSoportes::ejecutarAsignacion", ".*");
        }

    public TalkResponse ejecutarAsignacion(TalkRequest call) {
        String hashidOfrecimiento = getContext().getString(call.getChatId(), "hashid_ofrecimiento");
        String hashidPeticion = getContext().getString(call.getChatId(), "hashid_peticion");

        return TalkResponse.ofText(
            className(this), call.getChatId(),
            "Se asignado la /peticion_" + hashidPeticion + " al /ofrecimiento_" + hashidOfrecimiento +
                "\nSoporte: /soporte_" + helper.getHashidSoporte() +
                "\nGestionar /soportes" +
                "\n/menu principal"
            );
    }

    // BUSCAR SOPORTE

    public TalkResponse buscarSoporte(TalkRequest call) {
        return criterioBusqueda(call);
    }

    public TalkResponse criterioBusqueda(TalkRequest call) {
        String criterio = helper.construirTextoCriterio(TipoBusqueda.OFRECIMIENTO, call.getChatId(), this);

        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Fijar criterios:" +
                "\n* /nombre de usuario" +
                "\n* Usuario de /telegram" +
                "\n* /categoria de soporte" +
                "\n* Rango de /fechas" +
                "\n\nEl criterio actual de busqueda es:" +
                "\n" + (criterio.isEmpty() ? "NO HAY CRITERIOS FIJADOS" : criterio) +
                "\n\n/limpiar criterios de búsqueda" +
                "\n/buscar según los criterios fijados"
        )
        .withString("xarxa.XarxaSoportes::criterioNombre", "/nombre")
        .withString("xarxa.XarxaSoportes::criterioTelegram", "/telegram")
        .withString("xarxa.XarxaSoportes::criterioCategoria", "/categoria")
        .withString("xarxa.XarxaSoportes::criterioFechas", "/fechas")
        .withString("xarxa.XarxaSoportes::limpiarCriterio", "/limpiar")
        .withString("xarxa.XarxaSoportes::efectuarBusqueda", "/buscar");
    }

    public TalkResponse limpiarCriterio(TalkRequest call) {
        call.getInputAdapter().sendResponse(TalkResponse.ofText(className(this), call.getChatId(),
            "Se han limpiado los criterios de búsqueda"
        ));
        return buscarSoporte(call);
    }

    public TalkResponse criterioNombre(TalkRequest call) {
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Introduce el nombre de la persona o una parte del nombre:" +
                "\nIr a /criterios"
        )
        .withRegex("xarxa.XarxaSoportes::criterioNombreFijar", ".*");
    }

    public TalkResponse criterioNombreFijar(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        getContext().set(call.getChatId(), "ofrecimiento_nombre", call.getMatch().getRegexMatches().get(0));
        call.getInputAdapter().sendResponse(TalkResponse.ofText(className(this), call.getChatId(),
            "Se ha asignado el criterio de búsqueda por nombre de persona"
        ));
        return criterioBusqueda(call);
    }

    public TalkResponse criterioTelegram(TalkRequest call) {
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Introduce el usuario de Telegram de la persona:" +
                "\nIr a /criterios"
        )
        .withRegex("xarxa.XarxaSoportes::criterioTelegramFijar", ".*");
    }

    public TalkResponse criterioTelegramFijar(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        call.getInputAdapter().sendResponse(TalkResponse.ofText(className(this), call.getChatId(),
            "Se ha asignado el criterio de búsqueda por usuario de Telegram"
        ));
        getContext().set(call.getChatId(), "ofrecimiento_telegram", call.getMatch().getRegexMatches().get(0));
        return criterioBusqueda(call);
    }

    public TalkResponse criterioCategoria(TalkRequest call) {
        Map<String, String> categorias = helper.getCategorias();
        String textoCategorias = categorias.entrySet().stream()
                                           .map(entry -> "* /" + entry.getKey() + " " + entry.getValue())
                                           .collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Selecciona una categoría por la que quieras filtrar:\n" +
                textoCategorias +
                "\n\nIr a /criterios"
        )
        .withRegex("xarxa.XarxaSoportes::criterioCategoriaFijar", "/(.*)");
    }

    public TalkResponse criterioCategoriaFijar(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        getContext().set(call.getChatId(), "ofrecimiento_categoria", call.getMatch().getRegexMatches().get(1));
        call.getInputAdapter().sendResponse(TalkResponse.ofText(className(this), call.getChatId(),
            "Se ha asignado el criterio de búsqueda por categoría"
        ));
        return criterioBusqueda(call);
    }

    public TalkResponse criterioFechas(TalkRequest call) {
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Introduce la fecha inicial en formato DD-MM-AAAA (también puedes introducir /hoy o /ayer):" +
                "\nIr a /criterios"
        )
        .withRegex("xarxa.XarxaSoportes::criterioFechas2", XarxaHelper.REGEX_FECHA_CRITERIOS);
    }

    public TalkResponse criterioFechas2(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        String fechaDesde = helper.construirFecha(call.getUpdate().message().text(), call.getMatch().getRegexMatches(), 1, false, true);
        getContext().set(call.getChatId(), "ofrecimiento_fechaDesde", fechaDesde);
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Introduce la fecha final en formato DD-MM-AAAA (también puedes introducir /hoy o /ayer o escribe /ok para usar la misma fecha inicial):" +
                "\nIr a /criterios"
        )
        .withRegex("xarxa.XarxaSoportes::criterioFechasFijar", XarxaHelper.REGEX_FECHA_CRITERIOS_OK);
    }

    public TalkResponse criterioFechasFijar(TalkRequest call) {
        if (call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/criterios")) {
            return criterioBusqueda(call);
        }

        String fechaHasta = call.getMatch().getRegexMatches().get(0).equalsIgnoreCase("/ok") ?
            getContext().getString(call.getChatId(), "ofrecimiento_fechaDesde") :
            helper.construirFecha(call.getUpdate().message().text(), call.getMatch().getRegexMatches(), 1, false, true);
        getContext().set(call.getChatId(), "ofrecimiento_fechaHasta", fechaHasta);

        call.getInputAdapter().sendResponse(TalkResponse.ofText(className(this), call.getChatId(),
            "Se ha asignado el criterio de búsqueda por rango de fechas"
        ));
        return criterioBusqueda(call);
    }

    public TalkResponse efectuarBusqueda(TalkRequest call) {
        Map<String, String> ofrecimientos = helper.getOfrecimientosBuscados();
        String textoOfrecimientos = ofrecimientos.entrySet().stream().map(
            entry -> "* /ofrecimiento_" + entry.getKey() + " " + entry.getValue()
        ).collect(Collectors.joining("\n"));

        return TalkResponse.ofText(
                className(this), call.getChatId(),
            "Estos son los ofrecimientos encontrados según el criterio fijado:" +
                "\n\n" + textoOfrecimientos +
                "\n\n(Selecciona un ofrecimiento para tenerlo en memoria)" +
                "\nContinuar la /busqueda" +
                "\n/menu principal"
        )
        .withString("xarxa.XarxaSoportes::criterioBusqueda", "/busqueda");
    }

    // FICHA SOPORTE

    public TalkResponse soporte(TalkRequest call) {
        String hashidSoporte = call.getMatch().getRegexMatches().get(1);
        String fichaSoporte = helper.getFichaSoporte(hashidSoporte);

        if (fichaSoporte != null) {
            getContext().set(call.getChatId(), "hashid_soporte", hashidSoporte);
            call.getInputAdapter().sendResponse(TalkResponse.ofText(className(this), call.getChatId(),
                fichaSoporte
            ));
        }
        return TalkResponse.ofText(
                className(this), call.getChatId(),
            (fichaSoporte == null ?
                "NO SE HA ENCONTRADO UN SOPORTE CON EL CODIGO INDICADO" :
                "(Se ha guardado el soporte en la memoria)") +
                "\n/asignar soportes" +
                "\n/menu principal"
        )
        .withPreserveHandlers();
    }
}

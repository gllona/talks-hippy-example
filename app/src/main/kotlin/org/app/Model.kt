package org.app

import org.logicas.librerias.talks.api.ModelApi
import org.logicas.librerias.talks.api.TalksConfiguration

class Model private constructor(
    private val talksConfig: TalksConfiguration
) : ModelApi {

    companion object {
        private var instance: ModelApi? = null

        @JvmStatic
        @Synchronized
        fun getInstance(talksConfig: TalksConfiguration): ModelApi {
            if (instance == null) {
                instance = Model(talksConfig)
            }
            return instance as ModelApi
        }
    }

    init {
        talksConfig.talkManager // start the Talk Manager
    }

    override fun shutdown() {
        talksConfig.talkManager.shutDown() // this will shut down the DB connector also
    }
}

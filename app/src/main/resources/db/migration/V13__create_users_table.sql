CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    tg_chat_id BIGINT NULL DEFAULT NULL,
    tg_username VARCHAR(255) NULL DEFAULT NULL,
    email VARCHAR(255) NULL DEFAULT NULL,
    role VARCHAR(64) NOT NULL,
    scope VARCHAR(255) NULL DEFAULT NULL,
    visits INT NOT NULL DEFAULT 0,
    PRIMARY KEY(id),
    INDEX user_tg_chat_id_idx (tg_chat_id),
    INDEX user_tg_user_name (tg_username),
    INDEX user_role_idx (role),
    INDEX user_visits (visits)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

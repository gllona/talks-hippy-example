SET FOREIGN_KEY_CHECKS=0;

DROP TABLE resources_category;
DROP TABLE resources_edition;
DROP TABLE resources_feeder;
DROP TABLE resources_keyword;
DROP TABLE resources_keywording;
DROP TABLE resources_resource;
DROP TABLE resources_user;
DROP TABLE resources_validation;
DROP TABLE resources_validator;

DROP TABLE xarxa_telegram;
DROP TABLE xarxa_usuario;
DROP TABLE xarxa_entidad;
DROP TABLE xarxa_categoria;
DROP TABLE xarxa_ofrecimiento;
DROP TABLE xarxa_peticion;
DROP TABLE xarxa_soporte;
DROP TABLE xarxa_nota;

SET FOREIGN_KEY_CHECKS=1;

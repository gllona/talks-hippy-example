ALTER TABLE talk_contexts
    MODIFY session_id VARCHAR(255) NOT NULL;

ALTER TABLE talks_interactions
    MODIFY chat_id VARCHAR(255) NOT NULL,
    MODIFY message_id VARCHAR(255) NOT NULL;

RENAME TABLE telegram_message_sender_queue TO message_sender_queue;

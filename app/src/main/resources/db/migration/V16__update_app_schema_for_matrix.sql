ALTER TABLE users
    RENAME COLUMN tg_chat_id TO chat_id,
    RENAME COLUMN tg_username TO username;

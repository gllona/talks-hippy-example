ALTER TABLE message_sender_queue
    ADD sent_at TIMESTAMP(3) NULL;

ALTER TABLE talks_interactions
    MODIFY created_at TIMESTAMP(3) NULL,
    MODIFY fly_at TIMESTAMP(3) NULL;

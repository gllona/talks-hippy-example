CREATE TABLE talks_matrix_messages (
    id BIGINT NOT NULL AUTO_INCREMENT,
    type CHAR(8) NOT NULL,
    chat_id VARCHAR(255) NOT NULL,
    actions TEXT NULL,
    PRIMARY KEY(id),
    INDEX matrix_messages_type (type),
    INDEX matrix_messages_chat_id (chat_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

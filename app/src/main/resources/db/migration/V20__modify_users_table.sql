ALTER TABLE users
    MODIFY COLUMN chat_id VARCHAR(255) NULL DEFAULT NULL;

UPDATE users
    SET username = CONCAT('@', username);

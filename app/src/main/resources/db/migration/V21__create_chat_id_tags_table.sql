CREATE TABLE chat_id_tags (
    id BIGINT NOT NULL AUTO_INCREMENT,
    chat_id VARCHAR(255) NOT NULL,
    tag VARCHAR(255) NOT NULL,
    value VARCHAR(255) NOT NULL,
    PRIMARY KEY(id),
    INDEX chat_id_tags_chat_id (chat_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

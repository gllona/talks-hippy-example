ALTER TABLE talks_contexts
    ADD points_to INT NULL DEFAULT NULL,
    ADD level TINYINT NOT NULL DEFAULT 0,
    ADD code VARCHAR(255) NULL DEFAULT NULL,
    ADD code_set_at TIMESTAMP(3) NULL DEFAULT NULL,
    ADD next_talk_response MEDIUMTEXT NULL DEFAULT NULL,
    ADD UNIQUE INDEX talks_contexts_code (code),
    ADD INDEX talks_contexts_code_sent_at (code_set_at),
    ADD CONSTRAINT talks_contexts_points_to_fk FOREIGN KEY (points_to) REFERENCES talks_contexts(id) ON DELETE CASCADE;

DROP INDEX talks_contexts_code ON talks_contexts;

CREATE INDEX talks_contexts_code ON talks_contexts (code);

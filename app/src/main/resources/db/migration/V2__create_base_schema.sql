CREATE TABLE user (
    id INT NOT NULL AUTO_INCREMENT,
    channel_id VARCHAR(64) NOT NULL,
    channel_user_id VARCHAR(64) NOT NULL,
    channel_username VARCHAR(64) NOT NULL,
    channel_chat_id VARCHAR(255) NOT NULL,
    PRIMARY KEY (id),
    KEY user_k_channel_id (channel_id),
    KEY user_k_channel_user_id (channel_user_id),
    KEY user_k_channel_username (channel_username)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE TABLE category (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    allow_external_request BIT(1) NOT NULL,
    strategy VARCHAR (64) NOT NULL,
    strategy_params TEXT NOT NULL,
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE TABLE resource (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    description VARCHAR(255) DEFAULT NULL,
    link VARCHAR(255) NOT NULL,
    is_master BIT(1) NOT NULL,
    master_id INT DEFAULT NULL,
    category_id INT NOT NULL,
    strategy VARCHAR (64) NOT NULL,
    strategy_params TEXT NOT NULL,
    PRIMARY KEY (id),
    KEY resource_k_is_master (is_master),
    CONSTRAINT resource_fk_master_id FOREIGN KEY (master_id) REFERENCES resource (id),
    CONSTRAINT resource_fk_category_id FOREIGN KEY (category_id) REFERENCES category (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE TABLE validation (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    resource_id INT NOT NULL,
    is_granted BIT(1) NOT NULL,
    is_revoked BIT(1) NOT NULL,
    granted_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    revoked_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    strategy VARCHAR (64) NOT NULL,
    strategy_params TEXT NOT NULL,
    PRIMARY KEY (id),
    KEY validation_k_is_granted (is_granted),
    KEY validation_k_is_revoked (is_revoked),
    CONSTRAINT validation_fk_user_id FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT validation_fk_resource_id FOREIGN KEY (resource_id) REFERENCES resource (id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE TABLE edition (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    resource_id INT NOT NULL,
    type VARCHAR(64) NOT NULL,
    edited_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    CONSTRAINT edition_fk_user_id FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT edition_fk_resource_id FOREIGN KEY (resource_id) REFERENCES resource (id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE TABLE keyword (
    id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(64) NOT NULL,
    PRIMARY KEY (id),
    KEY keyword_k_name (name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE TABLE validator (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    category_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT validator_fk_user_id FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT validator_fk_category_id FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE TABLE feeder (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    category_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT feeder_fk_user_id FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT feeder_fk_category_id FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE TABLE keywording (
    id INT NOT NULL AUTO_INCREMENT,
    resource_id INT NOT NULL,
    keyword_id INT NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT keywording_fk_resource_id FOREIGN KEY (resource_id) REFERENCES resource (id) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT keywording_fk_keyword_id FOREIGN KEY (keyword_id) REFERENCES keyword (id) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

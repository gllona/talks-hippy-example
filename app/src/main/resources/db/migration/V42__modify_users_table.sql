ALTER TABLE users
    ADD COLUMN client_id INT NULL DEFAULT NULL AFTER scope,
    ADD INDEX users_client_id (client_id);

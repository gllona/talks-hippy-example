CREATE TABLE talk_contexts (
    id INT NOT NULL AUTO_INCREMENT,
    session_id INT NOT NULL,
    talk_name VARCHAR(255) NOT NULL,
    type VARCHAR(255) NOT NULL,
    content TEXT NOT NULL,
    PRIMARY KEY(id),
    UNIQUE KEY(session_id, talk_name, type)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE TABLE talks_media_resources (
    id BIGINT NOT NULL AUTO_INCREMENT,
    resource_id VARCHAR(255) NOT NULL,
    input_adapter VARCHAR(255) NOT NULL,
    file_id MEDIUMTEXT NULL DEFAULT NULL,
    updated_at TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    INDEX talks_media_resources_resource_id (resource_id),
    INDEX talks_media_resources_input_adapter (input_adapter)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

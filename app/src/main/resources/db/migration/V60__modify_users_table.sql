ALTER TABLE users
    ADD COLUMN phone_number VARCHAR(255) NULL DEFAULT NULL AFTER username,
    ADD INDEX users_phone_number (phone_number);

CREATE TABLE xarxa_telegram (
	id INT NOT NULL AUTO_INCREMENT,
	cuenta VARCHAR(255) NOT NULL,
	usuario VARCHAR(255) NOT NULL,
	PRIMARY KEY (id),
	INDEX xarxa_cuenta_adaptador_cuenta (cuenta),
	INDEX xarxa_cuenta_adaptador_usuario (usuario)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE TABLE xarxa_usuario (
	id INT NOT NULL AUTO_INCREMENT,
	id_telegram INT NULL DEFAULT NULL,
	nombre VARCHAR(255) NOT NULL,
	telefono_movil VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	direccion VARCHAR(255) NOT NULL,
	fecha_nacimiento DATE NOT NULL,
	es_administrador BIT(1) NOT NULL DEFAULT 0,
	es_coordinador BIT(1) NOT NULL DEFAULT 0,
	telegram_chat_id INT NULL,
	telegram_username VARCHAR(255) NOT NULL,
	PRIMARY KEY (id),
	INDEX xarxa_usuario_es_administrador (es_administrador),
	INDEX xarxa_usuario_es_coordinador (es_coordinador),
	INDEX xarxa_usuario_telegram_chat_id (telegram_chat_id),
	CONSTRAINT xarxa_usuario_fk_id_telegram
		FOREIGN KEY (id_telegram) REFERENCES xarxa_telegram (id)
		ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE TABLE xarxa_entidad (
	id INT NOT NULL AUTO_INCREMENT,
	nombre VARCHAR(255) NOT NULL,
	direccion VARCHAR(255) NULL,
	telefono_fijo VARCHAR(255) NULL,
	telefono_movil VARCHAR(255) NULL,
	email VARCHAR(255) NULL,
	id_usuario_representante INT NULL,
	PRIMARY KEY (id),
    CONSTRAINT xarxa_entidad_fk_id_usuario_representante
    	FOREIGN KEY (id_usuario_representante) REFERENCES xarxa_usuario (id)
    	ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE TABLE xarxa_categoria (
	id INT NOT NULL AUTO_INCREMENT,
	abreviatura VARCHAR(255) NOT NULL,
	nombre VARCHAR(255) NOT NULL,
	eliminada_en TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (id),
	INDEX xarxa_categoria_eliminada_en (eliminada_en)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE TABLE xarxa_ofrecimiento (
	id INT NOT NULL AUTO_INCREMENT,
	id_categoria INT NULL DEFAULT NULL,
	id_usuario_oferente INT NULL DEFAULT NULL,
	id_entidad_oferente INT NULL DEFAULT NULL,
	descripcion VARCHAR(255) NOT NULL,
	creado_en TIMESTAMP NOT NULL,
	expira_en TIMESTAMP NULL DEFAULT NULL,
	PRIMARY KEY (id),
	INDEX xarxa_ofrecimiento_creado_en (creado_en),
	INDEX xarxa_ofrecimiento_expira_en (expira_en),
	FULLTEXT (descripcion),
	CONSTRAINT xarxa_ofrecimiento_fk_id_categoria
		FOREIGN KEY (id_categoria) REFERENCES xarxa_categoria (id)
		ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT xarxa_ofrecimiento_fk_id_usuario_oferente
		FOREIGN KEY (id_usuario_oferente) REFERENCES xarxa_usuario (id)
		ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT xarxa_ofrecimiento_fk_id_entidad_oferente
		FOREIGN KEY (id_entidad_oferente) REFERENCES xarxa_entidad (id)
		ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE TABLE xarxa_peticion (
	id INT NOT NULL AUTO_INCREMENT,
	id_categoria INT NULL DEFAULT NULL,
	id_usuario_peticionario INT NOT NULL,
	descripcion VARCHAR(255) NOT NULL,
	creado_en TIMESTAMP NOT NULL,
	PRIMARY KEY (id),
	INDEX xarxa_peticion_creado_en (creado_en),
	FULLTEXT (descripcion),
	CONSTRAINT xarxa_peticion_fk_id_categoria
		FOREIGN KEY (id_categoria) REFERENCES xarxa_categoria (id)
		ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT xarxa_peticion_fk_id_usuario_peticionario
		FOREIGN KEY (id_usuario_peticionario) REFERENCES xarxa_usuario (id)
		ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE TABLE xarxa_soporte (
	id INT NOT NULL AUTO_INCREMENT,
	id_ofrecimiento INT NULL DEFAULT NULL,
	id_peticion INT NULL DEFAULT NULL,
	id_usuario_enlazador INT NOT NULL,
	descripcion VARCHAR(255) NULL,
	creado_en TIMESTAMP NOT NULL,
	PRIMARY KEY (id),
	INDEX xarxa_soporte_creado_en (creado_en),
	FULLTEXT (descripcion),
	CONSTRAINT xarxa_soporte_fk_id_ofrecimiento
		FOREIGN KEY (id_ofrecimiento) REFERENCES xarxa_ofrecimiento (id)
		ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT xarxa_soporte_fk_id_peticion
		FOREIGN KEY (id_peticion) REFERENCES xarxa_peticion (id)
		ON DELETE SET NULL ON UPDATE CASCADE,
	CONSTRAINT xarxa_soporte_fk_id_usuario_enlazador
		FOREIGN KEY (id_usuario_enlazador) REFERENCES xarxa_usuario (id)
		ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

CREATE TABLE xarxa_nota (
	id INT NOT NULL AUTO_INCREMENT,
	id_soporte INT NULL DEFAULT NULL,
	id_ofrecimiento INT NULL DEFAULT NULL,
	id_peticion INT NULL DEFAULT NULL,
	descripcion VARCHAR(255) NOT NULL,
	creado_en TIMESTAMP NOT NULL,
	PRIMARY KEY (id),
	INDEX xarxa_soporte_creado_en (creado_en),
	FULLTEXT (descripcion),
	CONSTRAINT xarxa_nota_fk_id_soporte
		FOREIGN KEY (id_soporte) REFERENCES xarxa_soporte (id)
		ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT xarxa_nota_fk_id_ofrecimiento
		FOREIGN KEY (id_ofrecimiento) REFERENCES xarxa_ofrecimiento (id)
		ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT xarxa_nota_fk_id_peticion
		FOREIGN KEY (id_peticion) REFERENCES xarxa_peticion (id)
		ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

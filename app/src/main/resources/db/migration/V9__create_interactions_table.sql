CREATE TABLE interactions(
    id BIGINT NOT NULL AUTO_INCREMENT,
    type CHAR(8), -- 'update' or 'message',
    created_at TIMESTAMP,
    PRIMARY KEY(id),
    INDEX interactions_created_at_idx (created_at)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1;

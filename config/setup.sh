#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd $SCRIPT_DIR

rm

cp config-app.properties.EXAMPLE config-app.properties

echo -n "Enable Telegram (y/n)? "
read yn
if [ "$yn" == "y" ]; then
  cp config-telegram.properties.EXAMPLE config-telegram.properties
fi

echo -n "Enable Matrix (y/n)? "
read yn
if [ "$yn" == "y" ]; then
  cp config-matrix.properties.EXAMPLE config-matrix.properties
fi

cd ../app
rm -rf config
ln -s ../config/ config
cd -

echo "Properties files were created in directory $SCRIPT_DIR"

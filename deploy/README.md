# How to deploy the bot as a `systemd` service

Follow this procedure to deploy the bot including healthcheck checks.

- install Java 17 and check the Java version with `java -version`
- install MariaDB
- create the database and the database user:
  - `sudo mariadb -u root -p`
  - `CREATE DATABASE bot;`
  - `CREATE USER 'bot'@'%' IDENTIFIED BY 'somesecurepassword';`
  - `GRANT ALL PRIVILEGES ON bot.* TO 'bot'@'%';`
  - `FLUSH PRIVILEGES;`
  - `exit;`
- create the bots Linux user with `sudo useradd -m -s /bin/bash bots`
- create the semi-privileged healthcheck Linux user with `sudo useradd -m -s /bin/bash botssudo`
- add privileges to restart services to the `botssudo` user:
  - `sudo visudo`
  - add at the end of the file a line with `botssudo ALL = NOPASSWD: /bin/systemctl`
  - save the file and exit the editor
- `cd` to the bot directory
- `config/setup.sh`
- create or edit the `.env` file, setting the needed environment variables
- `./gradlew build shadowJar` to build the fat JAR
- `sudo mkdir /var/log/bots`
- `sudo cp deploy/systemd/bot.service /etc/systemd/system`, then `sudo vi /etc/systemd/system/bot.service` and 
  edit all the paths to directories and the JAR
- `sudo cp deploy/systemd/bot-healthcheck.service /etc/systemd/system`, then `sudo vi /etc/systemd/system/bot-healthcheck.service` and
  edit all the paths to directories
- `sudo systemctl daemon-reload`
- `sudo systemctl start bot`, then `tail -f /var/log/bots/bot.log` and check that the bot starts correctly
- if everything is ok then `sudo systemctl enable bot`
- `sudo systemctl start bot-healthcheck.sh`, the `tail -f /var/log/bots/bot.log` and check that the bot healthcheck script
  works correctly
- if everything is ok then `sudo systemctl enable bot-healthcheck`

All done!

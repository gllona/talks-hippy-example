#!/usr/bin/env bash

# usage:
#
# healthcheck-systemd.sh <service-name> <max-load> <wait-time> <check-time> <samples>
#
# - service-name: the name of the systemd service you want to control
# - max-load: the level of CPU load that will make the controlled service to restart
# - wait-time: this script will wait the specified number of seconds before starting to monitor the controlled service
# - check-time: the intervals, in seconds, between two consecutive checks
# - samples: how much samples are taken for metrics
#
# the following variables should be defined in the ../.env file:
#
# - REST_CONTROLLER_PORT
# - HEALTH_ENDPOINTS_API_KEY
#
# the following variables should be defined in the ../config/config-app.properties file:
#
# - HEALTH_ENDPOINT

SERVICE=$1
MAX_LOAD=$2
WAIT_TIME=$3
CHECK_TIME=$4
SAMPLES=$5

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
PROTOCOL=http
SERVER=localhost
API_KEY=$(grep HEALTH_ENDPOINTS_API_KEY= $SCRIPT_DIR/../.env | cut -d= -f2)
PORT=$(grep REST_CONTROLLER_PORT= $SCRIPT_DIR/../.env | cut -d= -f2)
PATH=$(grep HEALTH_ENDPOINT= $SCRIPT_DIR/../config/config-app.properties | cut -d= -f2)
URL="$PROTOCOL://$SERVER:$PORT$PATH"
CORES=$(/usr/bin/cat /proc/cpuinfo | /usr/bin/grep processor | /usr/bin/wc -l)
FULL_LOAD=$MAX_LOAD   # FULL_LOAD=$(( $MAX_LOAD * $CORES ))
FULL_LOAD_ACCUM=$(( $FULL_LOAD * $SAMPLES ))
LOAD_THRESHOLD=$(( $MAX_LOAD * 110 / 100 ))

WAIT=1
LOAD_ACCUM=0
LOAD_LASTS=0; for ((i=1;i<$SAMPLES;i++)); do LOAD_LASTS="$LOAD_LASTS,0"; done

while(true); do

    if [[ $WAIT -eq 1 ]]; then
        WAIT=0
        echo "Healthcheck: waiting for service to start..."
        /usr/bin/sleep $WAIT_TIME
    fi

    # CPU load
    PID=$(/usr/bin/systemctl show --property MainPID --value $SERVICE)
    LOAD=$(/usr/bin/top -b -n 2 -d 0.2 -p $PID | /usr/bin/tail -1 | /usr/bin/awk '{print $9}' | /usr/bin/cut -d'.' -f1)
    LOAD_FOR_ACCUM=$LOAD; if [[ $LOAD_FOR_ACCUM -ge $LOAD_THRESHOLD ]]; then LOAD_FOR_ACCUM=$LOAD_THRESHOLD; fi
    LOAD_LAST=$(echo $LOAD_LASTS | /usr/bin/cut -f1 -d,)
    LOAD_LASTS_COPY=$LOAD_LASTS; LOAD_LASTS=$LOAD_FOR_ACCUM
    for ((i=$SAMPLES;i>1;i--)); do LOAD_I=$(echo $LOAD_LASTS_COPY | /usr/bin/cut -f$i -d,); LOAD_LASTS="$LOAD_I,$LOAD_LASTS"; done
    LOAD_ACCUM=$(( $LOAD_ACCUM - $LOAD_LAST + $LOAD_FOR_ACCUM ))
    if [[ $LOAD_ACCUM -ge $FULL_LOAD_ACCUM ]]; then RESTART_BY_LOAD=1; else RESTART_BY_LOAD=0; fi

    # healthcheck endpoint
    /usr/bin/wget --timeout=5 -O /dev/null --header="Authorization: Bearer $API_KEY" $URL 2>/dev/null
    STATUS=$?
    if [[ $STATUS -eq 0 ]]; then RESTART_BY_HEALTHCHECK=0; else RESTART_BY_HEALTHCHECK=1; fi

    NOW=$(/usr/bin/date --iso-8601=seconds | /usr/bin/cut -f1 -d+)
    echo "$NOW / Load: $LOAD - $LOAD_LASTS / Healthcheck: $STATUS"

    if [[ $RESTART_BY_LOAD -eq 0 && $RESTART_BY_HEALTHCHECK -eq 0 ]]; then
        /usr/bin/sleep $CHECK_TIME
    else
        WAIT=1
        LOAD_ACCUM=0; LOAD_LASTS=0; for ((i=1;i<$SAMPLES;i++)); do LOAD_LASTS="$LOAD_LASTS,0"; done
        echo "Healthcheck: restarting service..."
        /usr/bin/sudo /usr/bin/systemctl restart $SERVICE
    fi

done

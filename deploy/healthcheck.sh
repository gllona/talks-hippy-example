#!/usr/bin/env bash

WGET="/usr/bin/wget --timeout=5 -O /dev/null"
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
PROTOCOL=http
SERVER=localhost
API_KEY=$(grep HEALTH_ENDPOINTS_API_KEY= $SCRIPT_DIR/../.env | cut -d= -f2)
PORT=$(grep REST_CONTROLLER_PORT= $SCRIPT_DIR/../.env | cut -d= -f2)
PATH=$(grep HEALTH_ENDPOINT= $SCRIPT_DIR/../config.properties | cut -d= -f2)
URL="$PROTOCOL://$SERVER:$PORT$PATH"

exec $WGET --header="Authorization: Bearer $API_KEY" $URL 2>/dev/null
